<?php

 require_once ("models/user.php");
 require_once 'models/category.php';
 require_once 'models/brand.php';
 require_once 'models/product.php';
 require_once 'models/cart.php';
 require_once 'models/checkout.php';
 require_once ("views/top.php");
?>
 <link rel="icon" type="image/gif" href="images/favicon.gif"/>
 
<script>
 $(document).ready(function(){
    $('.tooltipped').tooltip({delay: 50});
  });
        
</script>
    </head>
    <body>
        <?php
       
       require_once ("views/header_top.php");
       require_once ("views/middle_header.php");
       require_once ("views/bootom_header.php");
       ?>
        <div class="page_body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="h2 text-center">Log In 
                <?php
   if (isset($_SESSION['msg'])) {
                                echo(" - " . $_SESSION['msg']);
                                unset($_SESSION['msg']);
                            }
                            if (isset($_SESSION['errors'])) {
                                $errors = $_SESSION['errors'];
                                unset($_SESSION['errors']);
                            }

                            if (isset($_SESSION['obj_user'])) {
                                $obj_user = unserialize($_SESSION['obj_user']);
                            } else {
                                $obj_user = new User();
                            }
    
    ?></h1>
        </div>
    </div>
                <div class="row">
        <div class="col-lg-offset-1 col-lg-11 col-md-offset-1 col-md-11 col-sm-offset-1 col-sm-11 col-xs-offset-1 col-xs-11" >
            <form method="post" action="process/process_login.php"enctype="multipart/form-data">
                <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8 cd">
                         <div class="row">
        <div class="input-field">
            
            <input  id="user_name" class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert Your User Name" name="user_name" type="text" value="<?php echo($obj_user->user_name)?>" />
          <label for="user_name">User Name</label>
          <span id="user_name_error">
              <?php
                if(isset($errors['user_name'])){
                    echo($errors['user_name']);
                }
                    
                ?> 
          </span>
        </div>
          </div>
                <div class="row">
        <div class="input-field ">
       
            <input id="password" name="password" class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert valid Password"type="password" />
               <label for="password">Password</label>
               <span id="password_error">
                   <?php
                if(isset($errors['password'])){
                    echo($errors['password']);
                }
                    
                ?> 
               </span>
        </div>
      </div>
                                    <div class="row">
        <div class="input-field ">
       
            <input id="remember" name="remember" class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert valid Remember"type="checkbox" />
               <label for="remember">Remember</label>
               <span id="remember">
                   <?php
                if(isset($errors['remember'])){
                    echo($errors['remember']);
                }
                    
                ?> 
               </span>
        </div>
      </div>
                    <div class="row">
                        <div class="input-field">
                            <button type="submit">Login</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
                </div>
        </div>
        <div class="footer"
        <?php
       require_once ("views/footer.php");
        ?>
        </div>
        <!--FOOTER ENDS -->
 