
<?php
 require_once ("models/user.php");
 require_once 'models/category.php';
 require_once 'models/brand.php';
 require_once 'models/product.php';
 require_once 'models/checkout.php';
 require_once 'models/cart.php';
 require_once ("views/top.php"); 

?> 
<?php
if ($obj_user->login) {
    echo('<title>' . $obj_user->first_name . ' || Home</title>');
} else {
    echo("<title>eShop || Home</title>");
}?>

</head>
<body>
                             <!--CONTAINER STARTS -->
                             
                         <div class="container-fluid" >
 <!--*********************************************PAGE HEADER************************************************************-->            
                               <!--PAGE HEADER STARTS -->
                         <header class="page_header">
                             
                                     <!--TOP HEADER STARTS -->
                                     <?php
                                     require_once ("views/header_top.php");
                                     ?>
                                     <!--TOP HEADER ENDS -->
                         
                                <!--MIDDLE HEADER STARTS -->
                                     <?php
                                     require_once ("views/middle_header.php");
                                     ?>
                                  <!--MIDDLE HEADER ENDS -->
                     
                                  <!--BOTTOM HEADER STARTS -->
                                   <?php
                                      require_once ("views/bootom_header.php");
                                    ?>
                          
                                    <!--BOTTOM HEADER ENDS -->

                                     <!--PAGE HEADER ENDS -->
                                    
 <!--*********************************************PAGE BODY************************************************************-->                                   
                                    <!--PAGE BODY STARTS --> 
                                      
                           <div class="page_body body" style="z-index: 0;">
                                        <!--SLIDER STARTS -->
                           <div class="row">
                            <?php 
                            require_once ("views/slider.php");
                             ?>
                           </div>
                            
                                        <!--SLIDER ENDS -->
                                      <?php
                                     require_once ("views/page_body.php");
                                      ?>
                                       
                           </div>
                                   
                                    <!--PAGE BODY ENDS --> 
<!--*********************************************PAGE FOOTER************************************************************-->
        

                                        <!--FOOTER STARTS -->
                           <?php
                           require_once ("views/footer.php");
                           ?>
                                        <!--FOOTERS ENDS -->

                               </div>
                                    <!--CONTAINER ENDS -->

 
 

        </body>
     

</html>
