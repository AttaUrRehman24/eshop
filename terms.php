<?php
 require_once ("models/user.php");
 require_once 'models/category.php';
 require_once 'models/brand.php';
 require_once 'models/checkout.php';
 require_once 'models/product.php';
 require_once 'models/cart.php';
 require_once ("views/top.php")?>
    </head>
    <body>
        <?php
       
       require_once ("views/header_top.php");
       require_once ("views/middle_header.php");
       require_once ("views/bootom_header.php");
       ?>
        <div class="page_body">
           <div class="row">
  		 
  
	   	<div id="sidebar-main" class="col-md-12">
	   		<div id="content">
	   		   			<div class="">
				<h1>Terms &amp; Conditions</h1>
				eshop.pk is an entirely third party business and we offer value product(s) and services on amazing discounts. These product(s) and goods are bought from reputed companies and vendors and some are specially imported from different countries.<br><br>By registering and buying the product(s) and services from eshop.pk means you agree to eshop.pk terms, conditions, refund, exchange and privacy policies. At any time if you do not agree with any terms or policies, you must discontinue using the website and its services and offers. It is compulsory for you to create an account by sign in to provide you the best services and offers of eshop.pk. The personal information you provide is utmost important for eshop.pk we have a strict privacy policy and never share it with any one at any stage. Other than as mentioned in our privacy policy. When you register with eshop.pk, we become authorized to send you the newsletters, promotional, marketing and other information via email. You may unsubscribe the eshop.pk from sending you the emails by using the unsubscribe option available in the email.<br><br>Any deal or product you buy from eshop.pk means you accept all terms and conditions about it. eshop.pk tries to provide you the best product and services but does not any responsibility or liability regarding the product(s) or services or deals. eshop.pk does not provide any warranty for these products, deals and services to any end user(s) of it. eshop.pk reserves the rights to block any user from using the website if any violation or misconduct happens from any user.<br><br>eshop.pk reserves the rights to get the compensation if any cost occur from the user(s) if they violate the laws of third party.<br><br>eshop.pk reserves the rights to change any terms and conditions and its policies at any stage without any prior notice. Feel free to contact eshop.pk for any inquiries or complaints at info@eshop.pk.<br>			</div>
			</div>
	   	</div> 
			</div>
</div>

        </div>
        <div class="footer"
        <?php
       require_once ("views/footer.php");
        ?>
        </div>
        <!--FOOTER ENDS -->
 