<?php
 require_once ("models/user.php");
 require_once 'models/category.php';
 require_once 'models/brand.php';
 require_once 'models/product.php';
 require_once 'models/checkout.php';
 require_once 'models/cart.php';
 require_once ("views/top.php");

 require_once "models/web_interface.php";
  require_once "views/top.php";
 ?>
<?php
if($obj_user->login){
    echo ("<title>'".$obj_user->first_name."|| My Account '</title>");
    
}
 else {
    echo "<title>My account</title>";    
}
?>
<script>
 $(document).ready(function(){
    $('.tooltipped').tooltip({delay: 50});
  });
        
</script>
    </head>
    <body>
        <?php
       
       require_once ("views/header_top.php");
       require_once ("views/middle_header.php");
       require_once ("views/bootom_header.php");
       ?>
        <div class="page_body">
           <?php
           if($obj_user->login){
               ?>
            <h1 class="h2 center-align">User Update Form
            <?php
              if (isset($_SESSION['msg'])) {
                            echo(" - " . $_SESSION['msg']);
                        }
                        if (isset($_SESSION['errors'])) {
                            $errors = $_SESSION['errors'];
                        }

                        if (isset($_SESSION['obj_user'])) {
                            $obj_user = unserialize($_SESSION['obj_user']);
                        } else {
                            
                        }
            
            ?>
            </h1>
            <div class="divider"></div>
            <div class="update_form">
                <?php
                            require_once "views/update_profile.php";
                ?>
            </div>
            <div class="row">
                <?php
                                require_once "views/myaccount_option.php";
                ?>
            </div>
            <?php
            }
            else {
    echo "<br><br><br><br><br><br>";
    echo "<h2 align='center'>Please login to view your profile</h2><br><br><br>";
    echo "<h2 align='center'>You are a new user to our site ?? <br> Get youself <a href='" . BASE_URL . "signup.php'> Register </a></h2><br><br>";
    echo "<h2 align='center'>Have account ?? <br> get yourself <a href='" . BASE_URL . "login_page.php'> Login </a></h2>";
    echo "<br><br><br><br><br><br>";
            }?>
        </div>
        <div class="footer"
        <?php
       require_once ("views/footer.php");
        ?>
        </div>
        <!--FOOTER ENDS -->
 