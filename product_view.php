<?php
require_once 'models/user.php';
require_once 'models/brand.php';
require_once 'models/product.php';
require_once 'models/cart.php';
require_once 'models/checkout.php';
require_once 'models/category.php';
 require_once "views/top.php";
 ?>
    </head>
    <body>
        <?php
       
       require_once ("views/header_top.php");
       require_once ("views/middle_header.php");
       require_once ("views/bootom_header.php");
       ?>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="body">
            <div class="row">
                
        <?php
        try{
             $start = isset($_GET['start']) ? $_GET['start'] : 0;
                            $count = isset($_GET['count']) ? $_GET['count'] : ITEM_PER_PAGE;
                            $type = isset($_GET['type']) ? $_GET['type'] : "all";
    $categoryID = isset($_GET['categoryID']) ? $_GET['categoryID'] : 0;
    $products= Product::get_products($categoryID,$start,$count);
    
    foreach ($products as $p){

        ?>
            
            <div class="col-sm-3"> 
            
                                      
                                           <div class="card">
                                               <div class="card-image waves-effect waves-block waves-light">
                         
                          <img class="img-responsive activator"  src="<?php echo(BASE_URL);?>Admin/products/<?php echo("$p->product_name/$p->product_image");?>" alt="<?php echo($p->product_name);?>">
                       
                           </div>
                           <div class="card-content">
                           <span class="card-title activator grey-text text-darken-4"><?php echo($p->product_name);?><i class="material-icons right fa fa-bars"></i></span>
        
                            <p>
                                <form action="process/process_cart.php" method="post">
                                    <input type="hidden" name="action" value="add_to_cart">
                                                    <input type="hidden" name="productID" value="<?php echo($p->productID); ?>">
                                                    
                                                    <button  type="submit" class="btn-floating fa fa-cart-arrow-down btn-sm  "></button><img src="images/icons/rating_star.png" alt="rating"/>
                                </form>
                                                    <div class="divider"></div>
                            <a href="<?php echo(BASE_URL);?>/product_detail.php?productID=<?php echo($p->productID);?>"><button class="btn-success btn-flat "><?php echo($p->product_price);?>&#8360</button></a><strike class="right center"><?php echo($p->product_discount);?>&#8360</strike></p>
                                 
                           </div>
                                               <div class="card-reveal" style="z-index: 1;">
                           <span class="card-title grey-text text-darken-5 h4">Description<i class="material-icons fa fa-times right "></i></span>
                           <div class="divider"></div>
                           <p class="text-muted text-info text-capitalize"><?php echo($p->proto_description); ?></p>
                           </div>
                           </div>
     </div>                                  
                      
                           
                <?php
            
    }?>
        </div>
             <?php
                            $pNUms = Product::pagination(ITEM_PER_PAGE, $categoryID);
                            echo "<center>";
                            echo "<ul class='pagination pagination-lg'> ";

                            foreach ($pNUms as $pNo => $index) {
                               
                                echo("<li><a href='".BASE_URL. "product_view.php?categoryID=$categoryID&start=$index'>$pNo</a></li>");
                            }

                            echo "</ul> ";

                            echo("</div>");
                            echo "</center>";
                        } catch (Exception $ex) {
                            echo($ex->getMessage());
                        }
                        ?>
            </div>
      
        <div class="footer">
        <?php
       require_once ("views/footer.php");
        ?>
        </div>
        <!--FOOTER ENDS -->
 