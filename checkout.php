<?php
 require_once ("models/user.php");
 require_once 'models/category.php';
 require_once 'models/brand.php';
 require_once 'models/product.php';
 require_once 'models/cart.php';
 require_once 'models/checkout.php';
 require_once 'models/web_interface.php';
 require_once ("views/top.php");
 if($obj_user->login){
     echo "<title>$obj_user->first_name - Checkout</title>";
 }
 
 ?>

    </head>
    <body>
        <?php
       
 
       require_once ("views/header_top.php");
       require_once ("views/middle_header.php");
       require_once ("views/bootom_header.php");
       ?>
        <div class="page_body">
            <div class="row">
                <?php
                if($obj_user->login){
     
                ?>
                <div class="col-lg-9 col-lg-offset-3  col-md-offset-3 col-md-9 col-sm-offset-3 col-sm-9 col-xs-5">
                    
               
                    <form  class="form-horizontal" action="process/process_checkout.php" method="post" enctype="multipart/form-data" novalidate >
          
                <?php
                try {

                    $key = $obj_user->userID;
//                    echo $key;
//                   die;
                    $user = User::get_UserInfo($key);
                    foreach ($user as $p) {
//                        echo "<pre>";
//                        print_r($user);
//                        echo "</pre>";
                        ?>
                        <!-- Form Name -->
                        
                        <div class="row">
                    <div class="input-field ">
                        <input type="text" id="first_name" name="first_name"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert valid Email" value="<?php echo($p->first_name)?>"/>
                        <label for="first_name">First Name</label>
                        <span id="email_error">
                            <?php
                if(isset($errors['first_name'])){
                    echo($errors['first_name']);
                }
                    
                ?> 
                        </span>
                    </div>
                </div>     
                        
                       <div class="row">
                    <div class="input-field ">
                        <input type="email" id="email" name="email"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert valid Email" value="<?php echo($p->email)?>"/>
                        <label for="email">Email</label>
                        <span id="email_error">
                            <?php
                if(isset($errors['email'])){
                    echo($errors['email']);
                }
                     
                ?> 
                        </span>
                    </div>
                </div>    
                        <div class="row">
                    <div class="input-field ">
                        <input type="text" id="contact" name="contact"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert valid Email" value="<?php echo($p->contact)?>"/>
                        <label for="email">Email</label>
                        <span id="contact_error">
                            <?php
                if(isset($errors['contact'])){
                    echo($errors['contact']);
                }
                     
                ?> 
                        </span>
                    </div>
                </div>   
                        <div class="row">
                    <div class="input-field ">
                        <input type="text" id="address" name="address"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert valid Email" value="<?php echo($p->address)?>"/>
                        <label for="addresss">Address</label>
                        <span id="address_error">
                            <?php
                if(isset($errors['address'])){
                    echo($errors['address']);
                }
                     
                ?> 
                        </span>
                    </div>
                </div>   
   <div class="row">
                    <div class="input-field ">
                        <label for="payment_method">Payment</label><br>
                       <?php
                              Web_Interface::load_payment_menthod($obj_checkout->payment_menthod);
                       ?>
                        <br>
                        <br>
                                          <span id="gender_error">
                                         <?php
                if(isset($errors['gender'])){
                    echo($errors['gender']);
                }
                    
                ?>     
     
   
                                          </span>

                    </div>
                    
                </div>
                        <button type="submit" value="CheckOut">CheckOut</button>
                    <?php }
                        
                    }
       catch (Exception $ex) {
           echo $ex->getMessage();
       }
                }
               else {
                   echo "<h3 class='center'> For Purchase </h3>";
                  echo"<br><br>";
                   echo"<h4 class='center'> Go Login <a href='".BASE_URL."/login.php'>Login</a></h4>";
 echo"<br><br>";
                   echo"<h4 class='center'> Go Login <a href='".BASE_URL."/signup.php'>Signup</a></h4>";
            }  
                
                
                ?>
        </form>
                     </div>
            </div>
        </div>
        <div class="footer">
        <?php
       require_once ("views/footer.php");
    
 ?>
        </div>
        <!--FOOTER ENDS -->
 