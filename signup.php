<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.

--><?php

 require_once ("models/user.php");
 require_once 'models/category.php';
 require_once 'models/brand.php';
 require_once 'models/product.php';
 require_once 'models/checkout.php';
 require_once 'models/cart.php';
 require_once 'models/web_interface.php';
 require_once ("views/top.php");
?>
 <link rel="icon" type="image/gif" href="images/favicon.gif"/>
 <link href="css/style.css" rel="stylesheet" type="text/css"/>
       <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 
  <link href="<?php echo(BASE_URL); ?>css/bootstrap.css" rel="stylesheet" type="text/css"/>
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function()

    {
      
    $('select').material_select();

        
        $('#country').on('change', function() {
        var country = $(this).val();  
        $.ajax({
               
               url: "fetch.php",
              method:"POST",
              
              data:{country:country},
            success: function(result){
            $("#state").html(result);
        }});
         });
//         $("#state").on('change',function(){
//             var state=$(this).val();
//             alert(state);
//             $.ajax({
//                 url:"dr.php",
//                 method:"POST",
//                 data:{state:state},
//                 success:function(result){
//                   $("#city").html(result)  ;
//                 }
//                 
//             });
//             
//         });
    });
</script>
<title>Eshop|Signup</title>
    </head>
    <body>
     <?php
       
       require_once ("views/header_top.php");
       require_once ("views/middle_header.php");
       require_once ("views/bootom_header.php");
       ?>
        <!--BODY STARTS -->
    <!--HEADER STARTS -->
    <div class="row">
        <div class="body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1 class="h2 text-center">User Registration
                <?php
    if(isset($_SESSION['msg'])){
         
        $msg=$_SESSION['msg'];
        unset($_SESSION['msg']);
        echo($msg);
        
        
        
    }
    if(isset($_SESSION['errors'])){
         
        $errors = $_SESSION['errors'];
        unset($_SESSION['errors']);
       
    }
    if(isset($_SESSION['obj_user'])){
        $obj_user = unserialize($_SESSION['obj_user']);
        unset($_SESSION['obj_user']);
    }
    else{
        $obj_user= new User();
    }
    
    
    ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-offset-1 col-lg-11 col-md-offset-1 col-md-11 col-sm-offset-1 col-sm-11 col-xs-offset-1 col-xs-11" >
            <form method="post" action="process/process_singup.php" id="user_form" class="form-horizontal"enctype="multipart/form-data" nonvalidate>
                <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8 cd">
                         <div class="row">
        <div class="input-field">
            
            <input  id="first_name" class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert Your Good Name" name="first_name" type="text" value="<?php echo($obj_user->first_name)?>" />
          <label for="first_name">First Name</label>
          <span id="first_name_error">
              <?php
                if(isset($errors['first_name'])){
                    echo($errors['first_name']);
                }
                    
                ?> 
          </span>
        </div>
                             <div id="atta">
                                 
                             </div>
          </div>
                <div class="row">
        <div class="input-field ">
       
            <input id="last_name" name="last_name" class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert valid Last Name"type="text" value="<?php echo($obj_user->last_name)?>" />
               <label for="last_name">Last Name</label>
               <span id="last_name_error">
                   <?php
                if(isset($errors['last_name'])){
                    echo($errors['last_name']);
                }
                    
                ?> 
               </span>
        </div>
      </div>
                <div class="row">
                    <div class="input-field ">
                        <input type="email" id="email" name="email"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert valid Email" value="<?php echo($obj_user->email)?>"/>
                        <label for="email">Email</label>
                        <span id="email_error">
                            <?php
                if(isset($errors['email'])){
                    echo($errors['email']);
                }
                    
                ?> 
                        </span>
                    </div>
                </div>    .
                <div class="row">
                    <div class="input-field ">
                        <input type="text" id="user_name" name="user_name"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Authentic User Name" value="<?php echo($obj_user->user_name)?>" />
                      <label for="user_name">User Name</label>
                      <span id="username_error">
                          <?php
                if(isset($errors['user_name'])){
                    echo($errors['user_name']);
                }
                    
                ?> 
                      </span>
                    </div>
                </div>
                 <div class="row">
                    <div class="input-field ">
                        <input type="password" id="password" name="password"  class="tooltipped" data-position="right" data-delay="50" data-tooltip="Insert secure and strong Password" />
                                          <label for="password">Password</label>
                                          <span id="password_error"><?php
                if(isset($errors['password'])){
                    echo($errors['password']);
                }
                    
                ?> </span>

                    </div>
                </div>
                <div class="row">
                    <div class="input-field">
                        <input type="password" id="confirm_password" name="confirm_password"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert Confirm Password" />
                                          <label for="confirm_password">Confirm Password</label>
                                          <span id="confirm_password_error">
                                              <?php
                if(isset($errors['confirm_password'])){
                    echo($errors['confirm_password']);
                }
                    
                ?> 
                                          </span>
                    </div>
                </div>
               
                <div class="row">
                    <div class="input-field ">
                        <input type="text" id="contact" name="contact" class=" tooltipped" data-position="right"data-delay="50" data-tooltip="Insert Yor Contact"  value="<?php echo($obj_user->contact)?>"/>
                                          <label for="contact">Contact</label>
                                          <span id="contact_error">
                                              <?php
                if(isset($errors['contact'])){
                    echo($errors['contact']);
                }
                    
                ?> 
                                          </span>

                    </div>
                </div>
                     <div class="row">
                    <div class="input-field ">
                        <label for="Gender"> Gender</label><br>
                       <?php
                              Web_Interface::load_genders($obj_user->gender);
                       ?>
                        <br>
                        <br>
                                          <span id="gender_error">
                                         <?php
                if(isset($errors['gender'])){
                    echo($errors['gender']);
                }
                    
                ?>     
     
   
                                          </span>

                    </div>
                    
                </div>
                 <div class="row">
                    <div class="input-field ">
                    
                        <label for="interest">Interest</label><br>
                        <?php
                                               Web_Interface::load_interests($obj_user->interest);
                        ?>
                        <br>
                        <br>
                                          <span id="interest_error">
                                              <?php
                if(isset($errors['interest'])){
                    echo($errors['interest']);
                }
                    
                ?> 
                                          </span>

                    </div>
                    
                </div>
                 <div class="row">
                
                             <div class="form-group ">
      <label class="control-label" for="date">
       Date of Birth
      </label>
                                 <input class="form-control" id="date" name="date" placeholder="MM/DD/YY" type="text" value="<?php echo($obj_user->date)?>"/>
       </div>
      
     
                       
                                       
                     <span id="dob_error">
                         <?php
                if(isset($errors['date'])){
                    echo($errors['date']);
                }
                    
                ?> 
                     </span>

                    
                    
                </div>
                <div class="row">
                    
<label>Country:</label><br/>

     <?php
   
   ?>
<div class="input-field">

   <?php
   Web_Interface::load_country();
   
   ?>


     
   
                               <span id="country_error">
                                   <?php
                if(isset($errors['country'])){
                    echo($errors['country']);
                }
                    
                ?> 
                               </span>

                  
                    
                </div>
                </div>
                
                  <div class="row">
                      <div class="form-group" id="state" name="state">
                                   
                 <label>State:</label><br/>
                    
                      </div>
                      <span id="state_error">
                          <?php
                if(isset($errors['state'])){
                    echo($errors['state']);
                }
                    
                ?> 
                      </span>
                    
                </div>
                <br>
                  <div class="row">
                      <div class="form-group"id="city" name="city" >
<!--                          <select  >
                              
                          </select>-->
                        <span id="city_error">
                            <?php
                if(isset($errors['city'])){
                    echo($errors['city']);
                }
                    
                ?> 
                        </span>
                    </div>
                </div>
                  <div class="row">
                    <div class="input-field">
                        <textarea class="materialize-textarea" id="address" name="address"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Your Currnet Residence address" value="<?php echo($obj_user->address)?>"></textarea>
                        <label for="address">Address</label>
                        <span id="address_error">
                            <?php
                if(isset($errors['address'])){
                    echo($errors['address']);
                }
                    
                ?> 
                        </span>
                        
                    </div>
                </div>
                <div class="row">
                       <div class="input-field">
      
         <input type="file" id="profile_image" name="profile_image" accept="image/gif, image/jpg, image/jpeg, image/png">
      
     
    </div>
                    <span class="error_image">
                     <?php
                //echo($obj_user->profile_image);
                if(isset($errors['profile_image'])){
                    echo($errors['profile_image']);
                }
                    
                ?>
                    </span>
                </div>
                                  <div class="row">
                   
                </div>
                                 
                  <div class="row">
                    <div class="input-field ">
                        <button type="submit" value="registration" class="btn-block">Submit</button>
                        
                    </div>
                </div>
                </div>
                
            </form>
        </div>
    </div>
    </div>
        <!--HEADER ENDS -->

        <!--BODY ENDS -->
        <!--FOOTER STARTS -->
        <div class="page-footer">     
   <?php
       require_once ("views/footer.php");
        ?>
        </div>
        <!--FOOTER ENDS -->


 <!--Date Picker script-->
<!-- Include jQuery -->
<!-- Include Date Range Picker -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
 
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script src="../js/bootstrap.min.js" type="text/javascript"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
	$(document).ready(function(){
		var date_input=$('input[name="date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'mm/dd/yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true
		});
	});
</script>
   <script>
     $(document).ready(function() {
        $('select').material_select();
    });
  </script>

<!--Date Picker script -->
    </body>
   
</html>
