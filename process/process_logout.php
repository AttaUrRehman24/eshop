<?php

session_start();
require_once '../models/user.php';
$errors = array();

$obj_user = unserialize($_SESSION['obj_user']);


try {

    $obj_user->logout();
    $_SESSION['msg'] = "You have been logged out "
            . "Thankyou for visiting us";
} catch (Exception $ex) {
    $_SESSION['msg'] = $ex->getMessage();
}
    header("Location:../index.php");
    unset($_SESSION['msg']);
?>