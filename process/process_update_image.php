<?php

session_start();
require_once '../models/cart.php';
require_once '../models/user.php';

$errors = array();
$obj_user=  unserialize($_SESSION['obj_user']);

try {
    $obj_user->profile_image = $_FILES['profile_image'];
}
catch (Exception $ex) {
    $errors['profile_image'] = $ex->getMessage();
}


if (count($errors) == 0) {

    try {
        
        $obj_user->update_profile_image($_FILES['profile_image']['tmp_name']);
        $_SESSION['msg'] = $msg;
        $msg = "Congratulations $obj_user->user_name your images have been update";
        header("Location:../myaccount.php");
    } catch (Exception $ex) {
        $_SESSION['msg_err'] = $ex->getMessage();
        
    }
    
} else {
    $msg = "Failed";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_user'] = serialize($obj_user);
    header("Location:../update_profile_image.php");
}
?>