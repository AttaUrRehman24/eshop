<?php
session_start();
require_once '../models/cart.php';

if(isset($_SESSION['obj_cart'])){
    $obj_cart = unserialize($_SESSION['obj_cart']);
}
else{
    $obj_cart = new Cart();
}

if(isset($_POST['action'])){
    
    switch ($_POST['action']) {
        case "add_to_cart":
            $item = new Item($_POST['productID']);
            $obj_cart->add_to_cart($item);

            break;


    }
}
else if(isset($_GET['action'])){
    switch ($_GET['action']) {
        case "remove_item":
            $item = new Item($_GET['productID']);
            $obj_cart->remove_item($item);

            break;
        case "empty_cart":
            $obj_cart->empty_cart();

            break;
    }
}


$_SESSION['obj_cart'] = serialize($obj_cart);

header("Location:". $_SERVER['HTTP_REFERER']);

?>