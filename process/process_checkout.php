<?php

session_start();
require_once '../models/checkout.php';
require_once '../models/cart.php';


$errors = array();
$obj_checkout = new Checkout();
$obj_cart =  new Cart();

$obj_cart->add_cart();


try {
    $obj_checkout->contact_number = $_POST['contact_number'];
}
catch (Exception $ex) {
    $errors['contact_number'] = $ex->getMessage();
}

try {
    $obj_checkout->zipcode = $_POST['zipcode'];
}
catch (Exception $ex) {
    $errors['zipcode'] = $ex->getMessage();
}


try {
    $obj_checkout->payment_menthod = isset($_POST['payment_menthod']) ? $_POST['payment_menthod'] : NULL;
}
catch (Exception $ex) {
    $errors['payment_menthod'] = $ex->getMessage();
}


try {
    $obj_checkout->customer_note = $_POST['customer_note'];
}
catch (Exception $ex) {
    $errors['customer_note'] = $ex->getMessage();
}



if (count($errors) == 0) {

    try {
        
        //$obj_checkout->add_checkout($act_code);
        $msg = "Congratulations";
        
        $_SESSION['msg_email'] = $msg_email;
        $_SESSION['msg'] = $msg;
        
        
    } catch (Exception $ex) {
        $_SESSION['msg_err'] = $ex->getMessage();
    }
    header("Location:../msg.php");
} else {
    $msg = "Failed";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_checkout'] = serialize($obj_checkout);
    header("Location:../checkout.php");
}
?>