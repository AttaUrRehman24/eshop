<?php
session_start();
include_once ("../models/user.php");
 /*echo("<pre>");
 print_r($_POST);
 echo("</pre>");*/
$errors =  array();

$obj_user = New User();
try {
    $obj_user->first_name = $_POST['first_name'];
} catch (Exception $ex) {
    $errors['first_name']= $ex->getMessage();
}
try {
    $obj_user->last_name = $_POST['last_name']; 
} catch (Exception $ex) {
   $errors['last_name']= $ex->getMessage();
   
}
try{
    $obj_user->email=$_POST['email'];
} catch (Exception $ex) {
$errors['email']=$ex->getMessage();
}
try{
    $obj_user->user_name=$_POST['user_name'];
} catch (Exception $ex) {
$errors['user_name']=$ex->getMessage();
}
try{
    $obj_user->password=$_POST['password'];
} catch (Exception $ex) {
$errors['password']=$ex->getMessage();
}

try{
    $obj_user->confirm_password=$_POST['confirm_password'];
} catch (Exception $ex) {
$errors['confirm_password']=$ex->getMessage();
}

try{
    $obj_user->contact=$_POST['contact'];
} catch (Exception $ex) {
$errors['contact']=$ex->getMessage();
        
}
try{
    $obj_user->address=$_POST['address'];
} catch (Exception $ex) {
$errors['address']=$ex->getMessage();
        
}
try{
    $obj_user->city=$_POST['city'];
} catch (Exception $ex) {
$errors['city']=$ex->getMessage();
        
}
try{
    $obj_user->state=$_POST['state'];
} catch (Exception $ex) {
$errors['state']=$ex->getMessage();
        
}
try {
    $obj_user->gender = $_POST['gender'];
} catch (Exception $ex) {
    $errors['gender']= $ex->getMessage();
}
try {
    $obj_user->interest=$_POST['interest'];
    
} catch (Exception $ex) {
    $errors['interest']=$ex->getMessage();
    
}
try {
    $obj_user->date=$_POST['date'];
} catch (Exception $ex) {
    $errors["date"]=$ex->getMessage();
}

try{
    $obj_user->country=$_POST['country'];
} catch (Exception $ex) {
$errors['country']=$ex->getMessage();
}

try{
    $obj_user->profile_image=$_FILES['profile_image'];

} catch (Exception $ex) {
$errors['profile_image']=$ex->getMessage();
}


//$errors[0] = "Invalid or missing First Name";

if(count($errors)== 0){
    $msg = "You have Succefully Sign up";
    $_SESSION['msg'] = $msg;
    try {
        $act_code = md5(crypt(rand()));
        $obj_user->add_user($act_code);
        $msg_email="Click here for<b><a href='http://localhost/eshop/activate.php?userID=$obj_user->userID&act_code=$act_code' target='_blank'>ACTIVATE</a></b>";
        $_SESSION['msg_email']=$msg_email;
        $obj_user->upload_profile_image($_FILES['profile_image']['tmp_name']);
    } catch (Exception $ex) {
         $_SESSION['msg_err']=$ex->getMessage();
    }
    $_SESSION['msg'] = $msg;
    header("Location:../welcome.php");
}
else{
    
    $msg = " __Sign up Failed";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_user'] = serialize($obj_user);
    header("Location:../signup.php");
}

?>