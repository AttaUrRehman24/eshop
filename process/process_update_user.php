<?php
session_start();
require_once ("../models/user.php");
$errors=  array();
$obj_user=  unserialize($_SESSION['obj_user']);
try {
    $obj_user->first_name = $_POST['first_name'];
} catch (Exception $ex) {
    $errors['first_name']= $ex->getMessage();
}
try {
    $obj_user->last_name = $_POST['last_name']; 
} catch (Exception $ex) {
   $errors['last_name']= $ex->getMessage();
   
}
try{
    $obj_user->email=$_POST['email'];
} catch (Exception $ex) {
$errors['email']=$ex->getMessage();
}
try{
    $obj_user->user_name=$_POST['user_name'];
} catch (Exception $ex) {
$errors['user_name']=$ex->getMessage();
}
try{
    $obj_user->contact=$_POST['contact'];
} catch (Exception $ex) {
$errors['contact']=$ex->getMessage();
        
}
try {
    $obj_user->gender = $_POST['gender'];
} catch (Exception $ex) {
    $errors['gender']= $ex->getMessage();
}


if(count($errors)==0)
{
    try{
        $obj_user->update_user($obj_user->userID);

        $msg="Your Update is Successfull $obj_user->user_name";
        $_SESSION['msg']=$msg;
    } catch (Exception $ex) {
$_SESSION['msg_err']=$ex->getMessage();
    }
    unset($_SESSION['msg']);
    header("Location:../myaccount.php");
}
 else {
$_SESSION['msg']="Update Failde";
$_SESSION['error']=$errors;
$_SESSION['obj_user']=  serialize($obj_user);
header("Location:../update_user.php");
}