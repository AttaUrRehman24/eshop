<?php
require_once 'models/user.php';
require_once 'models/brand.php';
require_once 'models/product.php';
require_once 'models/category.php';
require_once 'models/cart.php';
require_once 'models/checkout.php';
require_once 'models/web_interface.php';
 require_once "views/top.php";
 ?>
    </head>
    <body>
        <?php
       
       require_once ("views/header_top.php");
       require_once ("views/middle_header.php");
       require_once ("views/bootom_header.php");
       ?>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="body">
            
        <?php
              try {
                          
                            $productID = isset($_GET['productID']) ? $_GET['productID'] : 0;
                            $product=  Product::get_single_product($productID);
                         
                           
                        } catch (Exception $ex) {
                            echo($ex->getMessage());
                        }
       
        ?>
            
       
            <div class="row"style="background-color:#f9f9f9">
           <?php
            try {
            foreach ($product as $p) {


            ?>
              
                <div class="col-md-12 col-sm-12 col-xs-12" >
                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                 <img class="img-thumbnail img-responsive" src="<?php echo(BASE_URL);?>Admin/products/<?php echo("$p->product_name/$p->product_image");?>" alt="<?php echo($p->product_name);?>"/>
                 </div>
                 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                 <blockquote>
                     <h5>Product Description</h5>
				<p>
			<?php echo$p->description ?>
				</p> <small>Product is <cite>Authentic</cite></small>
			</blockquote>
                 </div>
                 <br>
                 <br>
<table class="table table-responsive  col-md-12 col-sm-8 col-xs-12 ">
				<thead>
					<tr>
						<th>
						Product Name
						</th>
						<th>
							Product Price
						</th>
						<th>
							Product Discount
						</th>
                                                <th>
                                                    Product Quantity
                                                </th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<?php echo "$p->product_name";?>
						</td>
						<td>
						<?php echo"$p->product_price";?>
						</td>
						<td>
							<?php echo"$p->product_discount";?>
						</td>
						<td>
							<?php echo" $p->quantity";?>
						</td>
					</tr>
									</tbody>
			</table>
                 
		</div>
            </div>
            <div class="row">

                <div class="col-md-offset-3 col-sm-offset-3 col-md-6 col-sm-6 ool-xs-12">
                    	<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						Order Form
					</h3>
				</div>
				<div class="panel-body">
					<form role="form">
				                <div class="row">
                    <div class="input-field">
                        <input type="text" id="full_name" name="full_name"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Full Name" />
                                          <label for="full_name">Full Name</label>
                                          <span id="full_name_error">
                                              <?php
                if(isset($errors['full_name'])){
                    echo($errors['full_name']);
                }
                    
                ?> 
                                          </span>
                    </div>
                </div>
							                <div class="row">
                    <div class="input-field">
                        <input type="email" id="email" name="email"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Full Name" />
                                          <label for="email">Email</label>
                                          <span id="email_error">
                                              <?php
                if(isset($errors['email'])){
                    echo($errors['email']);
                }
                    
                ?> 
                                          </span>
                    </div>
                </div>
                                            				                <div class="row">
                    <div class="input-field">
                        <input type="text" id="contact" name="contact"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Full Name" />
                                          <label for="full_name">Contact</label>
                                          <span id="contact_error">
                                              <?php
                if(isset($errors['contact'])){
                    echo($errors['contact']);
                }
                    
                ?> 
                                          </span>
                    </div>
                </div>
				                <div class="row">
                    <div class="input-field">

   <?php
   Web_Interface::load_country();
   
   ?>
                                          <label for="country">Country</label>
                                          <span id="country_error">
                                              <?php
                if(isset($errors['country'])){
                    echo($errors['country']);
                }
                    
                ?> 
                                          </span>
                    </div>
                </div>
				<div class="checkbox">
					 
					<label>
						<input type="checkbox" /> Check me out
					</label>
				</div> 
				<button type="submit" class="btn btn-default">
					Submit
				</button>
			</form>
                                    	<div class="btn-group dropup">
				 
				<button class="btn btn-default">
					Action
				</button> 
				<button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li>
						<a href="#">Action</a>
					</li>
					<li class="disabled">
						<a href="#">Another action</a>
					</li>
					<li class="divider">
					</li>
					<li>
						<a href="#">Something else here</a>
					</li>
				</ul>
			</div>


				</div>
				<div class="panel-footer">
				
				</div>

                </div>

<?php
    }
} catch (Exception $ex) {
 echo($ex->getMessage());
}
?>
           
            </div>
        </div>
           
      
        <div class="footer">
        <?php
       require_once ("views/footer.php");
        ?>
        </div>
        <!--FOOTER ENDS -->
 