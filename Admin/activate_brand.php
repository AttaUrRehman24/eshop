<?php
require_once "../models/admin.php";
require_once "../models/brand.php";
require_once "views/top.php";

?>
</head>
<body>
    <?php
    require_once "views/header.php";
    ?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <?php
  $obj_brand=new Brand();
  try{
 $brandID = isset($_GET['brandID']) ? $_GET['brandID'] : 0;
                        $act_code = isset($_GET['act_code']) ? $_GET['act_code'] : 0;
  $obj_brand->brandID=$brandID;
  $obj_brand->activate_brand($act_code);
   echo("Congratulations Brand Activated you can now enjoy our site ");
                    } catch (Exception $ex) {

                        echo($ex->getMessage());
                    }
  ?>
                        </h1>
                    
                    </div>
                </div>
                <div class="row">
		
	</div>
            </div>
        </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
