<?php
require_once '../models/admin.php';
require_once '../models/product.php';
require_once '../models/brand.php';
require_once '../models/category.php';
require_once 'views/top.php';
require_once '../models/web_interface.php';
?>
</head>
<body>
    <?php
 
    require_once "views/header.php";
    ?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <?php
                            if(isset($_SESSION['msg'])){
                                $msg=($_SESSION['msg']);
                                unset($_SESSION['msg']);
                                echo "$msg";
                            }
                              if(isset($_SESSION['errors'])){
         
        $errors = $_SESSION['errors'];
        unset($_SESSION['errors']);
       
    }
    $obj_admins=  Admin::get_sigle_admins($_GET['adminID']);
    foreach ($obj_admins as $obj_admin) {
    

                    
                            
                            ?>
                        </h1>
                    
                    </div>
                </div>
         <div class="row">
                    <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8"style="background-color: #cccccc; border-radius: 12px; padding: 30px;">

                        <form  id="form" action=<?php echo "process/process_update_admin.php?adminID=$obj_admin->adminID";?> method="post" enctype="multipart/form-data" nonvalidate>

                            <div class="form-group" >
                                <label>First Name</label>
                            
                                <input class="form-control" type="text" id="first_name" name="first_name" value="<?php echo($obj_admin->first_name)?>" placeholder="Enter First Name">
                                
                                <p id="first_name_error">
                                  <?php
                                 if(isset($errors['first_name'])){
                                     echo($errors['first_name']);
                                                    }
                                  ?>  
                                </p>
                            </div>
                            
                            <div class="form-group" >
                                <label>Last Name</label>
                                <input class="form-control" type="text" id="last_name" name="last_name" value="<?php echo($obj_admin->last_name)?>" placeholder="Enter Last Name">
                              
                                <span id="last_name_error">
                                  <?php
                                  if(isset($errors['last_name'])){
                                      echo ($errors['last_name']);
                                  }
                                  ?>  
                                </span>
                            </div>


                           <div class="form-group" >
                                <label>Contact</label>
                                <input class="form-control" type="text" id="contact" name="contact" value="<?php echo($obj_admin->contact)?>" placeholder="Enter Email Name">
                                
                                <span id="contact_error">
                                  <?php
                                  if(isset($errors['contact'])){
                                      echo ($errors['contact']);
                                  }
                                  ?>  
                                </span>
                            </div>
                            
                            <div class="form-group" >
                                <label>Email</label>
                                <input class="form-control" type="email" id="email" name="email" value="<?php echo($obj_admin->email)?>" placeholder="Enter Email Name">
                                
                                <span id="email_error">
                                  <?php
                                  if(isset($errors['email'])){
                                      echo ($errors['email']);
                                  }
                                  ?>  
                                </span>
                            </div>

                  
                                    
             
      
        
                     <?php
               
    }     
                ?>
                    
                
                            <div class="row">


 <button type="submit" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                           

                            </div>
                        </form>
                        <div class="copy_right bg-danger">
                        <p>©2016.www.eshop.pk|POWERED BY:ATTA UR REHMAN</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
