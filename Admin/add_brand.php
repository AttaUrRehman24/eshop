<?php
require_once "../models/admin.php";
require_once "../models/brand.php";
require_once "views/top.php";


?>
</head>
<title>Add Brand</title>
<body>
   
        <div id="page-wrapper">

            <div class="container-fluid">
<?php
require_once "views/header.php";
?>
                <!-- Page Heading -->
                <div class="row">
                     <?php
if ($obj_admin->login) {
    
?>
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add Brands
                                  <?php
                if (isset($_SESSION['msg'])) {
                    echo(" - " . $_SESSION['msg']);
                    unset($_SESSION['msg']);
                }
              
                if (isset($_SESSION['errors'])) {
                    $errors = $_SESSION['errors'];
                    unset($_SESSION['errors']);
                }

                if (isset($_SESSION['obj_brand'])) {
                   
                    $obj_brand = unserialize($_SESSION['obj_brand']);
                    
                } else {
                    $obj_brand = new Brand();
                }
                ?>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="admin_home.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Add Brand
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">

                        <form role="form" action="process/process_addbrand.php" method="post" enctype="multipart/form-data" nonvalidate>

                            <div class="form-group">
                                <label>Brand Name</label>
                                <input class="form-control" type="text" id="brand_name" name="brand_name" placeholder="Brand Name" value="<?php echo($obj_brand->brand_name);?>"/>
                                
                                <p class="help-block">Enter Brand Authentic brand name.</p>
                                <span class="brand_name">
                                <?php
             
                if(isset($errors['brand_name'])){
                    echo($errors['brand_name']);
                }
                    
                ?>     
                                </span>
                            </div>

                         
                       <div class="form-group">
                           <label>Add Brand Image</label>
         <input type="file" id="brand_image" name="brand_image" accept="image/gif, image/jpg, image/jpeg, image/png">
      
     <span class="error_image">
                     <?php
                //echo($obj_user->profile_image);
                if(isset($errors['brand_image'])){
                    echo($errors['brand_image']);
                }
                    
                ?>
                    </span>
    </div>
                    
               


 <button type="submit" class="btn btn-default">Submit Button</button>
 <button type="reset" class="btn btn-default">Reset Button</button>

                           




                        </form>
                                            <?php
                        
}

?>
 <div class="copy_right bg-danger">
                        <p>©2016.www.eshop.pk|POWERED BY:ATTA UR REHMAN</p>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
