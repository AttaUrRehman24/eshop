<?php
require_once '../models/admin.php';
require_once '../models/product.php';
require_once '../models/brand.php';
require_once '../models/category.php';
require_once 'views/top.php';
require_once '../models/web_interface.php';
?>
</head>
<body>
    <?php
    require_once "views/header.php";
    ?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header center">
                            <?php
                            if(isset($_SESSION['msg'])){
                                $msg=($_SESSION['msg']);
                                unset($_SESSION['msg']);
                                echo "$msg";
                            }
                              if(isset($_SESSION['errors'])){
         
        $errors = $_SESSION['errors'];
        unset($_SESSION['errors']);
       
    }
                            if(isset($_SESSION['obj_pro'])){
                                $obj_product=  unserialize($_SESSION['obj_pro']);
                                unset($_SESSION['obj_pro']);
                                
                            }
 else {
     $obj_product=new Product();
 }
                            
                            ?>
                            ADMINS 
              </h1>
                    </div>
                </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12 ">
                                <table class="table table-responsive table-bordered">
                                    <thead>
                                    <tr>
        <th>Product_Image</th>
        <th>Product_ID</th>
        <th>Product_Name</th>
        <th>Change Image</th>
        <th>Price</th>
        <th>Discount</th>
        <th>Quantity</th>
        <th>Proto_Description</th>
        
        <th>Update Product</th>
        
        </tr>
                                    </thead>
                                    
                                    <tbody>
                                    <?php  try {

$keypro = $_GET['productID'];
//            echo $keypro;
//            die;
            $products = Product::get_single_product($keypro);
            foreach ($products as $a) {

                echo("<tr align='center'>"
                . "<th><img style='max-width:100px;max-height:100px' src='" . BASE_URL . "products/$a->product_name/$a->product_image' alt='$a->product_name' class='brandsidepic'>"
              
                        . "</th>"
                . "<th>$a->productID</th>"
                . "<form method='post' action='" . BASE_URL . "process/process_update_product.php?productID=$a->productID' >"
                . "<th><div class='form-group'><input class='form-control' type='text' name='product_name' value='" . $a->product_name . "'></div></th>"
                 //."<th> <input type='file' name='product_image'id='product_image' class='btn btn-primary'/></th>"
                        . "<th><div class='form-group'><input class='form-control' type='text' name='product_discount' value='" . $a->product_discount . "'></div></th>"
                . "<th><div class='form-group'><input class='form-control'type='text' name='product_price' value='" . $a->product_price . "'></div></th>"
                . "<th><div class='form-group'><input class='form-control' type='text' name='quantity' value='" . $a->quantity . "'></div></th>"
                . "<th><textarea class='form-control' rows='7' cols='150' name='proto_description'> $a->proto_description  </textarea></th>"
                . "<th><input type='submit' class='btn btn-large btn-success'value='Update'></th>"
                . "</tr>");
                ?>
                                        
                                        <tr>
                                            <th>
                                       
                                                
                                            </th>
                                            <th></th>
                                            <th>
                                           <?php
                                            if(isset($errors['product_name'])){
                                      echo ($errors['product_name']);
                                  }
                                           ?> 
                                            </th>
                                            <th>
                                                           <?php
                                            if(isset($errors['product_image'])){
                                      echo ($errors['product_image']);
                                  }
                                           ?> 
                                            </th>
                                            <th>
                                                <?php
                                                
                                                if(isset($errors['product_price']))
                                                {
                                                    echo ($errors['product_price']);
                                                }
                                                ?>
                                            </th>
                                            
                                            <th>
                                                <?php
                                                if(isset($errors['product_discount']))
                                                {
                                                    echo ($errors['product_discount']);
                                                }
//                                                ?>
                                            </th>
                                             <th>
                                                <?php
                                                if(isset($errors['proto_description']))
                                                {
                                                    echo ($errors['proto_description']);
                                                }
                                                ?>
                                            </th>
                                             <th>
                                                <?php
                                                if(isset($errors['quantity']))
                                                {
                                                    echo ($errors['quantity']);
                                                }
                                                ?>
                                            </th>
                                        </tr>
                    <?php
              echo ("</form>");
            }
   
        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                     <?php
                        } catch (Exception $ex) {
                            echo($ex->getMessage());
                        }
                        ?>
                    </div>
                </div>
                
            
      
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
