<?php

require_once "../models/admin.php";
require_once "views/top.php";
require_once '../models/web_interface.php';



?>
  <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 
  
  <?php
 
if ($obj_admin->login) {
    echo('<title>' . $obj_admin->admin_name . ' || Home</title>');
} else {
    echo("<title>eShop_Home</title>");
}?>    
</head>

<body>

        <div id="page-wrapper">

            <div class="container-fluid">
                <div class="row">
<?php
require_once "views/header.php";
?>
                </div>
                <!-- Page Heading -->
                <div class="row">
                    <?php
if ($obj_admin->login) {
    
?>
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Admin Registration
                            <?php
                            if(isset($_SESSION['msg'])){
         
        $msg=$_SESSION['msg'];
        unset($_SESSION['msg']);
        echo($msg);
        
        
        
    }
    if(isset($_SESSION['errors'])){
         
        $errors = $_SESSION['errors'];
        unset($_SESSION['errors']);
       
    }
      if (isset($_SESSION['obj_adm'])) {
                    $obj_admin = unserialize($_SESSION['obj_adm']);
                } else {
                    $obj_admin = new Admin();
                }
   
                            ?>
                                         
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Add Admin
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8"style="background-color: #cccccc; border-radius: 12px; padding: 30px;">

                        <form  id="form" action="process/process_sigup.php" method="post" enctype="multipart/form-data" nonvalidate>

                            <div class="form-group" >
                                <label>First Name</label>
                            
                                <input class="form-control" type="text" id="first_name" name="first_name" value="<?php echo($obj_admin->first_name)?>" placeholder="Enter First Name">
                                
                                <p id="first_name_error">
                                  <?php
                                 if(isset($errors['first_name'])){
                                     echo($errors['first_name']);
                                                    }
                                  ?>  
                                </p>
                            </div>
                            
                            <div class="form-group" >
                                <label>Last Name</label>
                                <input class="form-control" type="text" id="last_name" name="last_name" value="<?php echo($obj_admin->last_name)?>" placeholder="Enter Last Name">
                              
                                <span id="last_name_error">
                                  <?php
                                  if(isset($errors['last_name'])){
                                      echo ($errors['last_name']);
                                  }
                                  ?>  
                                </span>
                            </div>

                            <div class="form-group" >
                                <label>Admin Name</label>
                                <input class="form-control" type="text" id="admin_name" name="admin_name" value="<?php echo($obj_admin->admin_name)?>" placeholder="Enter Admin Name">
                          
                                <span id="admin_name_error">
                                  <?php
                                  if(isset($errors['admin_name'])){
                                      echo ($errors['admin_name']);
                                  }
                                  ?>  
                                </span>
                            </div>

                           <div class="form-group" >
                                <label>Contact</label>
                                <input class="form-control" type="text" id="contact" name="contact" value="<?php echo($obj_admin->contact)?>" placeholder="Enter Email Name">
                                
                                <span id="contact_error">
                                  <?php
                                  if(isset($errors['contact'])){
                                      echo ($errors['contact']);
                                  }
                                  ?>  
                                </span>
                            </div>
                            
                            <div class="form-group" >
                                <label>Email</label>
                                <input class="form-control" type="email" id="email" name="email" value="<?php echo($obj_admin->email)?>" placeholder="Enter Email Name">
                                
                                <span id="email_error">
                                  <?php
                                  if(isset($errors['email'])){
                                      echo ($errors['email']);
                                  }
                                  ?>  
                                </span>
                            </div>

                            <div class="form-group" >
                                <label>Password</label>
                                <input class="form-control" type="password" id="password" name="password"  placeholder="Enter Password">
                           
                                <span id="password_error">
                                  <?php
                                  if(isset($errors['password'])){
                                      echo ($errors['password']);
                                  }
                                  ?>  
                                </span>
                            </div>

                            <div class="form-group" >
                                <label>City</label>
                                <input class="form-control" type="text" id="city" name="city" value="<?php echo($obj_admin->city)?>" placeholder="Enter City">
                               
                                <span id="city_error">
                                  <?php
                                  if(isset($errors['city'])){
                                      echo ($errors['city']);
                                  }
                                  ?>  
                                </span>
                            </div>


                              <div class="form-group" >
                                <label>Country</label>
                                <br>
                  <?php
                              Web_Interface::load_countries($obj_admin->country);
                       ?>
                            
                                <span id="country_error">
                                  <?php
                                  if(isset($errors['country'])){
                                      echo ($errors['country']);
                                  }
                                  ?>  
                                </span>
                            </div>
                            
                            <div class="form-group">
                                <label>State</label>
                                <input class="form-control" type="text" id="state" name="state" value="<?php echo($obj_admin->state)?>" placeholder="Enter State">
                             
                                <span id="state_error">
                                  <?php
                                  if(isset($errors['state'])){
                                      echo ($errors['state']);
                                  }
                                  ?>  
                                </span>
                            </div>
      <div class="form-group ">
      <label class="control-label" for="date">
       Date of Birth
      </label>
                                 <input class="form-control" id="date" name="date" placeholder="MM/DD/YY" type="text" value="<?php echo($obj_admin->date)?>"/>
       </div>
      
     
                       
                                       
                     <span id="dob_error">
                         <?php
                if(isset($errors['date'])){
                    echo($errors['date']);
                }
                    
                ?> 
                     </span>
                            
                           <div class="form-group" >
                                <label>Address</label>
                                <input class="form-control" type="text" id="address" name="address" value="<?php echo($obj_admin->address)?>" placeholder="Enter Email Name">
                                
                                <span id="address_error">
                                  <?php
                                  if(isset($errors['address'])){
                                      echo ($errors['address']);
                                  }
                                  ?>  
                                </span>
                            </div>
                            <div class="form-group" >
                                <label>Gender</label>
                                <br>
                  <?php
                              Web_Interface::load_genders($obj_admin->gender);
                       ?>
                              
                                <span id="gender_error">
                                  <?php
                                  if(isset($errors['gender'])){
                                      echo ($errors['gender']);
                                  }
                                  ?>  
                                </span>
                            </div>
                            <div class="form-group" >
                                <label>Interest</label>
                                <br>
                  <?php
                              Web_Interface::load_interests($obj_admin->interest);
                       ?>
                              
                                <span id="interest_error">
                                  <?php
                                  if(isset($errors['interest'])){
                                      echo ($errors['interest']);
                                  }
                                  ?>  
                                </span>
                            </div>
                          
                                    <div class="row">
             
      
         <input type="file" id="profile_image" name="profile_image" accept="image/gif, image/jpg, image/jpeg, image/png">
      
     
    </div>
                    <span class="error_image">
                     <?php
                //echo($obj_user->profile_image);
                if(isset($errors['profile_image'])){
                    echo($errors['profile_image']);
                }
                    
                ?>
                    </span>
                
                            <div class="row">


 <button type="submit" class="btn btn-default">Submit Button</button>
                            <button type="reset" class="btn btn-default">Reset Button</button>

                           

                            </div>
                        </form>
                        <?php
}
?>
                        <div class="copy_right bg-danger">
                        <p>©2016.www.eshop.pk|POWERED BY:ATTA UR REHMAN</p>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
<script src="../js/bootstrap.min.js" type="text/javascript"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
	$(document).ready(function(){
		var date_input=$('input[name="date"]'); //our date input has the name "date"
		var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
		date_input.datepicker({
			format: 'mm/dd/yyyy',
			container: container,
			todayHighlight: true,
			autoclose: true
		});
	});
</script>
<!--Date Picker script -->
</body>

</html>
