<?php
require_once '../models/admin.php';
require_once '../models/product.php';
require_once '../models/brand.php';
require_once '../models/category.php';
require_once 'views/top.php';
require_once '../models/web_interface.php';
?>
<title>Display Admin</title>
</head>
<body>
    <?php
    require_once "views/header.php";
    ?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                                        <?php
if ($obj_admin->login) {
    
?>
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            ADMINS 
              </h1>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                                <table class="table table-responsive  col-md-12 col-sm-8 col-xs-12 ">
                                    <thead>
                                    <tr>
        <th>Image</th>
        <th>AdminID</th>
        <th>Name</th>
        <th>Gender</th>
        <th>Email</th>
        <th>Country</th>
        <th>Date</th>
        <th>Contact</th>
        <th>Active</th>
        <th>Delete</th>
        <th>Update</th>
        </tr>
                                    </thead>
                                    <tbody>
                                    <?php  try {

            $admins = Admin::get_admins();
            foreach ($admins as $a) {
               
     echo("<tr align='center'>"
                . "<th><img  style='max-height:100px;max-width:100px;'src='" . BASE_URL . "admins/$a->admin_name/$a->profile_image' alt='$a->admin_name' class='brandsidepic'></th>"
                . "<th>$a->adminID</th>"
                . "<th>$a->first_name $a->last_name</th>"
                . "<th>$a->gender</th>"
                . "<th>$a->email</th>"
                . "<th>$a->country</th>"
                . "<th>$a->date</th>"
                . "<th>$a->contact</th>");
                if($a->is_active == 1){
                    echo "<th>Yes</th>";
                }
                else {
                    echo"<th>Yes</th>";
                }
                echo ("<td><a href='" . BASE_URL . "process/Remove.php?action=remove_admin&adminID=$a->adminID'>X</a></td>"
                . "<td><a href='" . BASE_URL . "update_admin.php?adminID=$a->adminID'>Edit</a></td>"
                . "</tr>");
            }
        } catch (Exception $ex) {
            echo($ex->getMessage());
        }  
        ?>
                                    </tbody>
                                </table>
                            </div>
                                                 <?php
                        
}
echo "<h1 class='center'> Your Are Not Login "
."<a href='".BASE_URL."/index.php' class='btn btn-danger' role='button'>LOGIN</a>"
."</h1>";
?>
                        </div>
                    
                    </div>
                </div>
                <div class="row">
		
	</div>
            </div>
        </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
