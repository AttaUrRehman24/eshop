<?php
require_once '../models/admin.php';
require_once '../models/brand.php';
require_once '../models/category.php';
require_once "views/top.php";
require_once '../models/product.php';

?>
<title>Add Product</title>
</head>
<body>
  
        <div id="page-wrapper">

            <div class="container-fluid">
<?php
require_once "views/header.php";
?>
  
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add Product
                                            <?php
                if (isset($_SESSION['msg'])) {
                    echo(" - " . $_SESSION['msg']);
                    unset($_SESSION['msg']);
                }
                
                if (isset($_SESSION['errors'])) {
                    $errors = $_SESSION['errors'];
                    unset($_SESSION['errors']);
                }

                if (isset($_SESSION['obj_pro'])) {
                    $obj_pro = unserialize($_SESSION['obj_pro']);
                     unset($_SESSION['obj_pro']);
                } else {
                    $obj_pro = new Product();
                }

                if (isset($_SESSION['obj_cat'])) {
                    $obj_category = unserialize($_SESSION['obj_cat']);
                    
                } else {
                    $obj_category = new Category();
                }
                ?>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="admin_home.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Add Product
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-offset-2 col-lg-8">

                        <form role="form" action="process/process_addproduct.php" method="post" enctype="multipart/form-data" novalidate>
                            <div class="row">
                           <div class="form-group  col-sm-8" >
                                <label>Product Name</label>
                                <input class="form-control" type="text" id="product_name" name="product_name" value="<?php echo($obj_pro->product_name);?>" placeholder="Enter Product Name">
                                
                                <span id="product_name">
                                  <?php
                                  if(isset($errors['product_name'])){
                                      echo ($errors['product_name']);
                                  }
                                  ?>  
                                </span>
                            </div>
                            </div>
                            <div class="row">
                             <div class="form-group col-sm-6" >
                                <label>Product Price</label>
                                <input class="form-control" type="text" id="product_price" name="product_price" value="<?php echo($obj_pro->product_price);?>" placeholder="Enter Product Price">
                               
                                <span id="product_price_error">
                                  <?php
                                  if(isset($errors['product_price'])){
                                      echo ($errors['product_price']);
                                  }
                                  ?>  
                                </span>
                            </div>
                            
                             <div class="form-group col-sm-6" >
                                <label>Product Discount</label>
                                <input class="form-control" type="text" id="product_discount" name="product_discount" value="<?php echo($obj_pro->product_discount);?>" placeholder="Enter Product Price">
                              
                                <span id="product_discount_error">
                                  <?php
                                  if(isset($errors['product_discount'])){
                                      echo ($errors['product_discount']);
                                  }
                                  ?>  
                                </span>
                            </div>
                            </div>
                            <div class="row">
                             <div class="form-group col-sm-5" >
                                <label>Product Quantity</label>
                                <input class="form-control" type="text" id="quantity" name="quantity" value="<?php echo($obj_pro->quantity);?>" placeholder="Enter Product Quantity">
                              
                                <span id="product_quantity_error">
                                  <?php
                                  if(isset($errors['quantity'])){
                                      echo ($errors['quantity']);
                                  }
                                  ?>  
                                </span>
                            </div>
                        </div>
                            <div class="row">
                             <div class="form-group col-sm-12" >
                                <label>Product Description</label>
                                <textarea class="form-control" type="text" id="description" name="description" value="<?php echo($obj_pro->description);?>" placeholder="Enter Product Description..........."></textarea>
                              
                                <span id="product_description_error">
                                  <?php
                                  if(isset($errors['description'])){
                                      echo ($errors['description']);
                                  }
                                  ?>  
                                </span>
                            </div>
                            </div>
                            <div class="row">
                             <div class="form-group col-sm-12" >
                                <label>Proto Description</label>
                                <textarea class="form-control" type="text" id="proto_description" name="proto_description" value="<?php echo($obj_pro->proto_description);?>" placeholder="Enter Product Description..........."></textarea>
                               
                                <span id="proto_description">
                                  <?php
                                  if(isset($errors['proto_description'])){
                                      echo ($errors['proto_description']);
                                  }
                                  ?>  
                                </span>
                            </div>
                            </div>
                            <div class="row">
                             <div class="form-group col-sm-6" >
                                <label>Product Category</label>
                                <br>
                  <?php 
                 
                                                    try {
                                                    
                                                        echo"<select class='form-control' id='category' name='category'>";
                                                        echo"<option  disabled selected value='Select Category'>Select Category</option>";
                                                        $categorys=  Category::get_category();
                                                        foreach ($categorys as $cat){
                                                            echo"<option value='$cat->categoryID'>$cat->category_name</option>";
                                                        }
                                                         echo"</select>";
                                                    } catch (Exception $ex) {
                                                        echo ("<option>".$ex->getMessage()."</option>");
                                                    }
                             
                            
                       ?>
                                <p class="help-block">Example block-level help text here.</p>
                                <span id="gender_error">
                                  <?php
                                  if(isset($errors['category'])){
                                      echo ($errors['category']);
                                  }
                                  ?>  
                                </span>
                            </div>
    <div class="form-group col-sm-6" >
                                <label>Product Brand</label>
                                
                                    <?php
                                  try {
                                                    
                                                        echo"<select class='form-control' id='brand' name='brand'>";
                                                        echo"<option  disabled  selected value='Select Brands'>Select Brands</option>";
                                                        $brands= Brand::get_brands();
                                                        foreach ($brands as $b){
                                                            echo"<option value='$b->brandID'>$b->brand_name</option>";
                                                        }
                                                         echo"</select>";
                                                    } catch (Exception $ex) {
                                                        echo ("<option>".$ex->getMessage()."</option>");
                                                    }
                         ?>
                                
                                <p class="help-block">Example block-level help text here.</p>
                                <span id="proto_description">
                                  <?php
                                  if(isset($errors['brand'])){
                                      echo ($errors['brand']);
                                  }
                                  ?>  
                                </span>
                            </div>
                            </div>
                            <div class="row">
                                <div class="form-group ">
                                    <input type="file" class="btn btn-flat"id="product_image" name="product_image" accept="image/gif, image/jpg, image/jpeg, image/png">
                    <span id="profile_image_error">
                        <?php
                        if (isset($errors['product_image'])) {
                            echo($errors['product_image']);
                        }
                        ?>
                    </span>
                                </div>
                            </div>
                            <div class="row">
                            <button type="submit" class="btn btn-success pull-right">Submit Product</button>
 <button type="reset" class="btn btn-danger pull-left">Reset Inputs</button>
                            </div>

                        </form>

                        
                    </div>
                     </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
