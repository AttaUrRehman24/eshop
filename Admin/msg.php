<?php
require_once "../models/admin.php";
require_once "../models/brand.php";
require_once "views/top.php";


?>
</head>
<body>
    <?php
    require_once "views/header.php";
    ?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <?php
        if (isset($_SESSION['msg'])) {
            echo($_SESSION['msg'] . "<hr>");
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msg_email'])) {
            echo($_SESSION['msg_email'] . "<hr>");
            unset($_SESSION['msg_email']);
        }

        if (isset($_SESSION['msg_err'])) {
            echo($_SESSION['msg_err'] . "<hr>");
            unset($_SESSION['msg_err']);
        }
        ?>
                        </h1>
                    
                    </div>
                </div>
                <div class="row">
		<div class="col-md-12">
			<div class="jumbotron">
				<h2>
					Welcome, new admin!
				</h2>
				<p>
                                    Welcoming a new co-worker or a  new employee in a company is truly a good gesture. <br>You are enrolled in this orqanization ,A better job and a better pay, your destiny has finally showed you the way.<br> A new job is not just about perfect timing, it is about celebrating a wonderful new beginning.<br>You work as an Admin on this Site  
				</p>
				<p>
					<a class="btn btn-primary btn-large" href="<?php BASE_URL?>/admin_home.php">Click Here for Activation Account</a>
				</p>
			</div>
		</div>
	</div>
            </div>
        </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
