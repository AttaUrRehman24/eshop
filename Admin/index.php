<?php
require_once "../models/admin.php";
require_once "views/top.php";
?>
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
</head>

<body>
  <div class="wrapper">
	<div class="container-fluid">
            <br><br>
            <br>
            
                <br>  
         <div class="row">
                <div id="login-overlay" class="modal-dialog">
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title" id="myModalLabel">ADMIN PANEL
                                            <?php
   if(isset($_SESSION['msg'])){
         
        $msg=$_SESSION['msg'];
        unset($_SESSION['msg']);
        echo($msg);
        
        
        
    }
    if(isset($_SESSION['errors'])){
         
        $errors = $_SESSION['errors'];
        unset($_SESSION['errors']);
       
    }
   
    
    ?>
              </h4>
          </div>
          <div class="modal-body">
              <div class="row">
                  <div class="col-xs-6">
                      <div class="well">
                                       <form   id="loginForm" action="process/process_login.php" method="post" enctype="multipart/form-data" >
                              <div class="form-group">
                                 <label>Admin Name</label>
                                <input class="form-control" type="text" id="admin_name" name="admin_name" value="<?php echo($obj_admin->admin_name)?>" placeholder="Enter Admin Name">

                                  <span class="help-block">
 <?php
                                  if(isset($errors['admin_name'])){
                                      echo ($errors['admin_name']);
                                  }
                                  ?>  

</span>
                              </div>
                              <div class="form-group">
               <label>Password</label>
               <input class="form-control" type="password" id="password" name="password"  placeholder="Enter Password">

                                     </div>
                                           <span class="help-block">  <?php
                                  if(isset($errors['password'])){
                                      echo ($errors['password']);
                                  }
                                  ?>  
                              </span>
                              <div class="checkbox">
                                  <label>
                                      <input type="checkbox" name="remember" id="remember"> Remember login
                                  </label>
                                                               </div>
                              <button type="submit" class="btn btn-success btn-block">Login</button>
                             
                          </form>
                      </div>
                  </div>
                  <div class="col-xs-6">
                      <p class="lead">Instruction<span class="text-success">POLICY</span></p>
                      <ul class="list-unstyled" style="line-height: 2">
                          <li><span class="fa fa-check text-success"></span> See all products </li>
                          <li><span class="fa fa-check text-success"></span> Update Products</li>
                          <li><span class="fa fa-check text-success"></span> Control Web Application</li>
                          <li><span class="fa fa-check text-success"></span> See checkout</li>
                          <li><span class="fa fa-close text-success"></span> Non-Members</li>
                      </ul>
                      <p><a href="privacy_policy.php" class="btn btn-info btn-block">Policy & instruction</a></p>
                  </div>
              </div>
          </div>
      </div>
  </div>
            </div>
	</div>

	</ul>
</div>

</body>
</html>
