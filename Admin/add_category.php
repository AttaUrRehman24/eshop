<?php
require_once '../models/admin.php';
require_once '../models/brand.php';
require_once '../models/category.php';
require_once "views/top.php";
?>
</head>
<title>Add category</title>
<body>
   
        <div id="page-wrapper">

            <div class="container-fluid">
<?php
require_once "views/header.php";
?>
                <!-- Page Heading -->
                <div class="row">
                                      <?php
if ($obj_admin->login) {
    
?>
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Add Category
                            <?php
                            if(isset($_SESSION['errors']))
                            {
                                $errors=$_SESSION['errors'];
                                unset($_SESSION['errors']);
                            }
                            if(isset($_SESSION['msg']))
                                {
                                echo ($_SESSION['msg']);
                                
                            }
                            if(isset($_SESSION['obj_cat']))
                            {
                              $obj_cat=  unserialize($_SESSION['obj_cat']);
                            }
 else {
     $obj_cat=new Category();
 }
                            
                            ?>
                            
                            
                            
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="admin_home.php">Dashboard</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i> Add Category
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">

                        <form role="form" action="process/process_addcategory.php" method="post" enctype="multipart/form-data" novalidate>

                            <div class="form-group">
                                <label>Category Name</label>
                                <input class="form-control" type="text" id="category_name" name="category_name" placeholder="Category Name" value="<?php echo($obj_cat->category_name);?>">
                                <p class="help-block">Enter Brand Authentic brand name.</p>
                                <span class="category_name_error">
                                    <?php
                                    if(isset($errors['category_name']))
                                    {
                                        echo ($errors['category_name']);
                                    }
                                    ?>
                                </span>
                            </div>

                        
                    
               


 <button type="submit" class="btn btn-default">Submit Button</button>
 <button type="reset" class="btn btn-default">Reset Button</button>

                           




                        </form>
                                            <?php
                        
}
echo "<h1 class='center'> Your Are Not Login "
."<a href='".BASE_URL."/index.php' class='btn btn-danger' role='button'>LOGIN</a>"
."</h1>";
?>
 <div class="copy_right bg-danger">
                        <p>©2016.www.eshop.pk|POWERED BY:ATTA UR REHMAN</p>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
