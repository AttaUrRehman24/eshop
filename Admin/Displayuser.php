<?php
require_once '../models/admin.php';
require_once '../models/user.php';
require_once '../models/product.php';
require_once '../models/brand.php';
require_once '../models/category.php';
require_once 'views/top.php';
require_once '../models/web_interface.php';
?>
<title>Display User</title>
</head>
<body>
    <?php
    require_once "views/header.php";
    ?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                     <?php
if ($obj_admin->login) {
    
?>
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            USERS
              </h1>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-sm-12">
                                <table class="table table-responsive  col-md-12 col-sm-8 col-xs-12 ">
                                    <thead>
                                    <tr>
        <th>Image</th>
        <th>User-ID</th>
        <th>User Name</th>
        <th>Name</th>
        <th>Gender</th>
        <th>Email</th>
        <th>Country</th>
        <th>Date</th>
        <th>Contact</th>
        <th>Active </th>
        <th>Delete </th>
                                    </thead>
                                    <tbody >
                                    <?php  try {

            $user = User::get_users();
            foreach ($user as $a) {
//                echo "<pre>";
//                print_r($a);
//                echo "</pre>";
     echo("<tr align='center'>"
                . "<th><img style='max-height:80px;max-width:80px;'src='../../users/$a->user_name/$a->profile_image' alt='$a->user_name' class='brandsidepic'/></th>"
                . "<th>$a->userID</th>"
             . "<th>$a->user_name</th>"
                . "<th>$a->first_name $a->last_name</th>"
                . "<th>$a->gender</th>"
                . "<th>$a->email</th>"
                . "<th>$a->country</th>"
                . "<th>$a->date</th>"
                . "<th>$a->contact</th>");
                ?>
                    <?php
                if($a->is_active == 1){
                    echo "<th>Yes</th>";
                }
                else {
                    echo"<th>Yes</th>";
                }?>
                                        <?php
                echo ("<td><a href='" . BASE_URL . "process/Remove.php?action=remove_user&userID=$a->userID'>X</a></td>"
                
                . "</tr>");
            }
        } catch (Exception $ex) {
            echo($ex->getMessage());
        }  
        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    
                    </div>
                                         <?php
                        
}

?>
                </div>
                <div class="row">
		
	</div>
            </div>
        </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
