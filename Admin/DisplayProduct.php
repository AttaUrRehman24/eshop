<?php
require_once '../models/admin.php';
require_once '../models/product.php';
require_once '../models/brand.php';
require_once '../models/category.php';
require_once 'views/top.php';
require_once '../models/web_interface.php';
?>
</head>
<title>Display Products</title>
<body>
    <?php
    require_once "views/header.php";
    ?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
 
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <h1 class="page-header">
                            ADMINS 
              </h1>
                         </div>
                </div>
                        <div class="row">
                            <div class="col-md-8 col-sm-8">
                                <table class="table table-bordered">
                                    <thead >
                                    <tr>
        <th>Product_Image</th>
        <th>Product_ID</th>
        <th>Product_Name</th>
        <th>Price</th>
        <th>Discount</th>
        <th>Proto_Description</th>
        <th>Quantity</th>
        <th>Action</th>
        <th>Update</th>
        
        </tr>
                                    </thead>
                                    <tbody >
                                    <?php  try {
$start = isset($_GET['start']) ? $_GET['start'] : 0;
                            $count = isset($_GET['count']) ? $_GET['count'] : ITEM_PER_PAGE;
                           
    $products= Product::get_products_for_admin($start,$count);
    
    foreach ($products as $a){
     echo("<tr align='center'>"
                              . "<th><img style='max-height:100px;max-width:100px'src='" . BASE_URL . "products/$a->product_name/$a->product_image' alt='$a->product_name' class='brandsidepic'></th>"
                . "<th>$a->productID</th>"
                . "<th>$a->product_name</th>"
                . "<th>$a->product_price</th>"
                
                . "<th>$a->product_discount</th>");
               
            $des=$a->proto_description;
            
           echo ("<th style='padding:0px;margin:0px;'>".wordwrap($des,20,"<br>")."</th>"
                
               ."<th>$a->quantity</th>"
                . "<td><a href='" . BASE_URL . "process/Remove.php?action=remove_product&productID=$a->productID'>X</a></td>"
                . "<td><a href='" . BASE_URL . "update_product.php?productID=$a->productID'>Edit</a></td>"
                . "</tr>");
            }
//                if($u->is_active == 1){
//                    . "<th>Yes</th>"
//                }else {
//                    . "<th>Yes</th>"
//                }
   
        ?>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        
                     <?php
                            $pNUms = Product::pagination_for_admin(ITEM_PER_PAGE);
                            echo "<center>";
                            echo "<ul class='pagination pagination-lg'> ";

                            foreach ($pNUms as $pNo => $index) {
                               
                                echo("<li><a href='".BASE_URL. "DisplayProduct.php?start=$index'>$pNo</a></li>");
                            }

                            echo "</ul> ";

                            echo("</div>");
                            echo "</center>";
                        } catch (Exception $ex) {
                            echo($ex->getMessage());
                        }
                        ?>
                   

            </div>
        </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
