<?php
require_once '../models/admin.php';
require_once '../models/product.php';
require_once '../models/brand.php';
require_once '../models/category.php';
require_once 'views/top.php';
require_once '../models/web_interface.php';
?>
</head>

    <?php
    if($obj_admin->login){
        echo "<title>".$obj_admin->admin_name."</title>";
    }
    ?>

<body>
    <?php
 
    require_once "views/header.php";
    ?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           <?php
                            if(isset($_SESSION['msg'])){
                                $msg=($_SESSION['msg']);
                                unset($_SESSION['msg']);
                                echo "$msg";
                            }
                              if(isset($_SESSION['errors'])){
         
        $errors = $_SESSION['errors'];
        unset($_SESSION['errors']);
       
    }
    $obj_admins=  Admin::get_sigle_admins($_GET['adminID']);
    foreach ($obj_admins as $obj_admin) {
    
//
//        echo "<pre>";
//        print_r($obj_admin);
//        echo "</pre>";
//                            
                            ?>
                        </h1>
                    
                    </div>
                </div>
                <div class="jumbotron">
                    <?php echo("<img style='max-width:150px;max-height:150px; float:left;' class='img-circle img-responsive img-rounded'src='".BASE_URL."/admins/$obj_admin->admin_name/$obj_admin->profile_image' alt='$obj_admin->admin_name'>");?>
                    <?php echo("<h1 style='margin-left:30%;'>$obj_admin->first_name $obj_admin->last_name</h1>");
                    
                    
                    ?>
                    <br>
                    <br><br>
                    <div class="modal-content">
                        <div class="modal-body">
                            <h4>Email</h4>
                            <?php echo($obj_admin->email);?>
                              <h4>Contact</h4>
                            <?php echo($obj_admin->contact);?>
                            
                              <h4>Gender</h4>
                            <?php echo($obj_admin->gender);?>
                              <h4>Country</h4>
                            <?php echo($obj_admin->country);?>
                              <h4>Address</h4>
                              <blockquote class="blockquote"><?php echo($obj_admin->address);?></blockquote>
                              <a class="btn btn-info" role="button" href="update_admin.php?adminID=<?php echo($obj_admin->adminID);?>">Update Account</a>
                        </div>
                    </div>
                    <?php
    }?>
                    
                </div>
            </div>
        </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>


