<?php

session_start();
require_once '../../model/brand.php';
require_once '../../model/admin.php';
require_once '../../model/user.php';
require_once '../../model/category.php';




$errors = array();
$obj_brand  = new Brand();

//$obj_brand= unserialize($_SESSION['obj_brand']);

//echo ($_SESSION['obj_brand']);
//die;

try {
    $obj_brand->brand_name = $_POST['brand_name'];
}
catch (Exception $ex) {
    $errors['brand_name'] = $ex->getMessage();
}


if (count($errors) == 0) {

    try {
        $key = $_GET['brandID'];
//        echo $key;
//        die;
        $obj_brand->update_brand($key);
        $msg = "Update information for $obj_brand->brand_name successfull !!!";
        
        $_SESSION['msg'] = $msg;
        
    } catch (Exception $ex) {
        $_SESSION['msg_err'] = $ex->getMessage();
    }
    header("Location:../DisplayBrands.php");
    unset($_SESSION['msg']);
} else {
    $msg = "Failed";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_user'] = serialize($obj_brand);
    header("Location:../UpdateBrands.php");
}
?>