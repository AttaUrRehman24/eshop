<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
require_once '../../models/product.php';
$errors= array();
$obj_pro= new Product();

try {
    $obj_pro->product_name = $_POST['product_name'];
} catch (Exception $ex) {
    $errors['product_name'] = $ex->getMessage();
}


try {
    $obj_pro->description = $_POST['description'];
} catch (Exception $ex) {
    $errors['description'] = $ex->getMessage();
}


try {
    $obj_pro->proto_description = $_POST['proto_description'];
} catch (Exception $ex) {
    $errors['proto_description'] = $ex->getMessage();
}


try {
    $obj_pro->product_price = $_POST['product_price'];
} catch (Exception $ex) {
    $errors['product_price'] = $ex->getMessage();
}


try {
    $obj_pro->quantity = $_POST['quantity'];
} catch (Exception $ex) {
    $errors['quantity'] = $ex->getMessage();
}

try {
    $obj_pro->product_discount = $_POST['product_discount'];
} catch (Exception $ex) {
    $errors['product_discount'] = $ex->getMessage();
}


try {
    $obj_pro->product_image = $_FILES['product_image'];
} catch (Exception $ex) {
    $errors['product_image'] = $ex->getMessage();
}
try {
    $obj_pro->category = $_POST['category'];
} catch (Exception $ex) {
    $errors['category'] = $ex->getMessage();
}
try {
    $obj_pro->brand = $_POST['brand'];
} catch (Exception $ex) {
    $errors['brand'] = $ex->getMessage();
}
if(count($errors)==0){
    try {
    $obj_pro->add_product();
$msg="Your $obj_pro->product_name is Added";    
$_SESSION['msg']=$msg;
$obj_pro->upload_product_image($_FILES['product_image']['tmp_name']);
    } catch (Exception $ex) {
        $_SESSION['msg_err']=$ex->getMessage();
    }
    header("Location:../displaymsg.php");
}
 else {
    $msg = "Failed";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_pro'] = serialize($obj_pro);
    header("Location:../add_product.php");
}