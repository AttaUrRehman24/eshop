<?php

session_start();
require_once '../../models/brand.php';

$errors = array();
$obj_brand = new Brand();


try {
    $obj_brand->brand_name = $_POST['brand_name'];
}
catch (Exception $ex) {
    $errors['brand_name'] = $ex->getMessage();
}


try {
    $obj_brand->brand_image = $_FILES['brand_image'];
}
catch (Exception $ex) {
    $errors['brand_image'] = $ex->getMessage();
}


if (count($errors) == 0) {

    try {
        
        $act_code = md5(crypt(rand()));
        $obj_brand->add_brand($act_code);
        $msg = "Congratulations $obj_brand->brand_name is now added in database";
        
        $msg_email = "Click to <a href='http://localhost/eshop/Admin/activate_brand.php?brandID=$obj_brand->brandID&act_code=$act_code' target='_blank'>ACTIVATE</a>";
        
        $_SESSION['msg_email'] = $msg_email;
        $_SESSION['msg'] = $msg;
        
        $obj_brand->upload_brand_image($_FILES['brand_image']['tmp_name']);
        
        
    } catch (Exception $ex) {
        $_SESSION['msg_err'] = $ex->getMessage();
    }
    header("Location:../msg.php");
} else {
    $msg = "Failed";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_brand'] = serialize($obj_brand);
    header("Location:../add_brand.php");
}
?>