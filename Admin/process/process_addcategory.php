<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
require_once '../../models/category.php';
$errors=array();
$obj_cat= new Category();

try {
$obj_cat->category_name=$_POST['category_name'];    
} catch (Exception $ex) {
    $errors['category_name']=$ex->getMessage();
}


if(count($errors)== 0)
{
    try {
       $obj_cat->add_category();
       $msg="Succesfully Added  $obj_cat->category_name in database";
    } catch (Exception $ex) {
        $_SESSION['msg_err']=$ex->getMessage();
    }
    $_SESSION['msg']=$msg;
    header("Location:../msg.php");
}
 else {
$msg="Failed";
$_SESSION['msg']=$msg;
$_SESSION['errors']=$errors;
$_SESSION['obj_cat']=  serialize($obj_cat);
header("Location:../add_category.php");
}
