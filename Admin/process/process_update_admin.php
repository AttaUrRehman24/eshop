<?php
session_start();
require_once ("../../models/admin.php");
$errors=  array();
$obj_admin=new Admin();
$key=$_GET[adminID];
try {
    $obj_admin->first_name = $_POST['first_name'];
} catch (Exception $ex) {
    $errors['first_name']= $ex->getMessage();
}
try {
    $obj_admin->last_name = $_POST['last_name']; 
} catch (Exception $ex) {
   $errors['last_name']= $ex->getMessage();
   
}
try{
    $obj_admin->email=$_POST['email'];
} catch (Exception $ex) {
$errors['email']=$ex->getMessage();
}

try{
    $obj_admin->contact=$_POST['contact'];
} catch (Exception $ex) {
$errors['contact']=$ex->getMessage();
        
}



if(count($errors)==0)
{
    try{
        $obj_admin->update_admin($key);

        $msg="Your Update is Successfull $obj_admin->admin_name";
        $_SESSION['msg']=$msg;
    } catch (Exception $ex) {
$_SESSION['msg_err']=$ex->getMessage();
    }
    unset($_SESSION['msg']);
    header("Location:../admin_account.php?adminID=$key");
}
 else {
$_SESSION['msg']="Update Failde";
$_SESSION['error']=$errors;
$_SESSION['admin']=  serialize($obj_admin);
header("Location:../update_admin.php?adminID=$key");
}