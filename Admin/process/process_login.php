<?php
session_start();
require_once '../../models/admin.php';
$errors = array();
$obj_admin = new Admin();


try {
    $obj_admin->admin_name = $_POST['admin_name'];
}
catch (Exception $ex) {
    $errors['admin_name'] = $ex->getMessage();
}

try {
    $obj_admin->password = $_POST['password'];
}
catch (Exception $ex) {
    $errors['password'] = $ex->getMessage();
}

if(count($errors)==0)
  
{
    
    try{
        $remember=(isset($_POST['remember']))?TRUE :FALSE;
        $obj_admin->login($remember);
        header("Location:../admin_account.php?adminID=$obj_admin->adminID");
    } catch (Exception $ex) {
$_SESSION['msg']=$ex->getMessage();
header("Location:../index_1.php");
    }
   
     
}
 else {
     $msg = "Failed";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors'] = $errors;
    $_SESSION['obj_admin'] = serialize($obj_admin);
    header("Location:../index_1.php");
}
?>