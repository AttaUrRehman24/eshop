<?php
session_start();
require_once "../../models/admin.php";
$errors=array();
$obj_admin=new Admin();
try {
    $obj_admin->first_name = $_POST['first_name'];
}
catch (Exception $ex) {
    $errors['first_name'] = $ex->getMessage();
}

try {
    $obj_admin->last_name = $_POST['last_name'];
}
catch (Exception $ex) {
    $errors['last_name'] = $ex->getMessage();
}

try {
    $obj_admin->email = $_POST['email'];
}
catch (Exception $ex) {
    $errors['email'] = $ex->getMessage();
}

try {
    $obj_admin->admin_name = $_POST['admin_name'];
}
catch (Exception $ex) {
    $errors['admin_name'] = $ex->getMessage();
}

try {
    $obj_admin->password = $_POST['password'];
}
catch (Exception $ex) {
    $errors['password'] = $ex->getMessage();
}

try {
    $obj_admin->contact = $_POST['contact'];
}
catch (Exception $ex) {
    $errors['contact'] = $ex->getMessage();
}

try {
    $obj_admin->gender = $_POST['gender'];
}
catch (Exception $ex) {
    $errors['gender'] = $ex->getMessage();
}

try {
    $obj_admin->interest = $_POST['interest'];
}
catch (Exception $ex) {
    $errors['interest'] = $ex->getMessage();
}

try {
    $obj_admin->date=$_POST['date'];
} catch (Exception $ex) {
    
    $errors['date']=$ex->getMessage();
}

try {
    $obj_admin->address = $_POST['address'];
}
catch (Exception $ex) {
    $errors['address'] = $ex->getMessage();
}

try {
    $obj_admin->city = $_POST['city'];
}
catch (Exception $ex) {
    $errors['city'] = $ex->getMessage();
}

try {
    $obj_admin->state = $_POST['state'];
}
catch (Exception $ex) {
    $errors['state'] = $ex->getMessage();
}

try {
    $obj_admin->country = $_POST['country'];
}
catch (Exception $ex) {
    $errors['country'] = $ex->getMessage();
}
try{
    $obj_admin->profile_image=$_FILES['profile_image'];

} catch (Exception $ex) {
$errors['profile_image']=$ex->getMessage();
}





if(count($errors)==0){
    try{
       $act_code = md5(crypt(rand()));
    $obj_admin->add_admin($act_code);
    $msg="Welcome MR/MRS $obj_admin->admin_name";
    $msg_email="Click Here For Activation <a href='http://localhost/eshop/Admin/msg.php??adminID=$obj_admin->adminID&act_code=$act_code' target='_blank'>ACTIVATE</a>";

    
    $obj_admin->upload_profile_image($_FILES['profile_image']['tmp_name']);
    $_SESSION['msg']=$msg;
$_SESSION['msg_email']=$msg_email;
    }
catch (Exception $ex) {
         $_SESSION['msg_err']=$ex->getMessage();
    }
    $_SESSION['msg'] = $msg;
header("Location:../msg.php");

    
}
else{
    $msg = "Failed";
    $_SESSION['msg'] = $msg;
    $_SESSION['errors']=$errors;
    $_SESSION['obj_adm']=  serialize($obj_admin);
    header("Location:../add_admin.php");
}