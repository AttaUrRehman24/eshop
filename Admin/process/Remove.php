<?php

session_start();
require_once '../../models/user.php';
require_once '../../models/admin.php';
require_once '../../models/brand.php';
require_once '../../models/product.php';
require_once '../../models/category.php';

if (isset($_SESSION['obj_user'])) {
    $obj_user = unserialize($_SESSION['obj_user']);
} else {
    $obj_user = new User();
}

if (isset($_SESSION['obj_cat'])) {
    $obj_cat = unserialize($_SESSION['obj_cat']);
} else {
    $obj_cat = new Category();
}

if (isset($_SESSION['obj_admin'])) {
    $obj_admin = unserialize($_SESSION['obj_admin']);
} else {
    $obj_admin = new Admin();
}

if (isset($_SESSION['obj_pro'])) {
    $obj_pro = unserialize($_SESSION['obj_pro']);
} else {
    $obj_pro = new Product();
}

if (isset($_SESSION['obj_brand'])) {
    $obj_brand = unserialize($_SESSION['obj_brand']);
} else {
    $obj_brand = new Brand();
}

if (isset($_POST['action'])) {

    switch ($_POST['action']) {
//        case "update_brand":
//            header("Location:" . BASE_URL . "UpdateBrands.php");
//            break;
//
//        case "update_admin":
//            // $obj_admin-> ;
//            break;
    }
} else if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case "remove_user":
            $key = ($_GET['userID']);
            $obj_user->delete_user($key);

            break;

        case "remove_admin":
            $key = ($_GET['adminID']);
            $obj_admin->delete_admin($key);

            break;

        case "remove_category":
            $key = ($_GET['categoryID']);
            $obj_cat->delete_category($key);

            break;

        case "remove_product":
            $key = ($_GET['productID']);
            $obj_pro->delete_product($key);

            break;

        case "remove_brand":
            $key = ($_GET['brandID']);
            $obj_brand->delete_brand($key);

            break;

        case "update_brand":
            header("Location:" . BASE_URL . "UpdateBrands.php");
            break;
       
            
    }
}

$_SESSION['obj_user'] = serialize($obj_user);
$_SESSION['obj_admin'] = serialize($obj_admin);
$_SESSION['obj_cat'] = serialize($obj_cat);
$_SESSION['obj_pro'] = serialize($obj_pro);
$_SESSION['obj_brand'] = serialize($obj_brand);


header("Location:" . $_SERVER['HTTP_REFERER']);
?>