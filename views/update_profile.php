<div class="row">
        <div class="col-lg-offset-1 col-lg-11 col-md-offset-1 col-md-11 col-sm-offset-1 col-sm-11 col-xs-offset-1 col-xs-11" >
            <form method="post" action="<?php echo(BASE_URL); ?>process/process_update_user.php" id="form_1116036" class="form-horizontal"enctype="multipart/form-data" nonvalidate>
                <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8 col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8 cd">
                         <div class="row">
        <div class="input-field">
            
            <input  id="first_name" class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert Your Good Name" name="first_name" type="text" value="<?php echo($obj_user->first_name)?>" />
          <label for="first_name">First Name</label>
          <span id="first_name_error">
              <?php
                if(isset($errors['first_name'])){
                    echo($errors['first_name']);
                }
                    
                ?> 
          </span>
        </div>
          </div>
                    <div class="row">
                    <div class="input-field">
                        <input type="text" id="last_name" name="last_name" class="tooltipped" data-position="right" data-delay="50" data-tootip="Your last name" value="<?php echo($obj_user->last_name)?>"/>
                        <label for="last_name">Last Name</label>
                        <span id="last_name_error">
                            <?php
                            if(isset($errors['last_name'])){
                                echo $errors['last_name'];
                            }
                                ?>
                        </span>
                    </div>
                    </div>
                       <div class="row">
                    <div class="input-field ">
                        <input type="text" id="user_name" name="user_name"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Authentic User Name" value="<?php echo($obj_user->user_name)?>" />
                      <label for="user_name">User Name</label>
                      <span id="username_error">
                          <?php
                if(isset($errors['user_name'])){
                    echo($errors['user_name']);
                }
                    
                ?> 
                      </span>
                    </div>
                </div>
                      <div class="row">
                    <div class="input-field ">
                        <label for="Gender"> Gender</label><br>
                       <?php
                              Web_Interface::load_genders($obj_user->gender);
                       ?>
                        <br>
                        <br>
                                          <span id="gender_error">
                                         <?php
                if(isset($errors['gender'])){
                    echo($errors['gender']);
                }
                    
                ?>     
     
   
                                          </span>

                    </div>
                    
                </div>
                     <div class="row">
                    <div class="input-field ">
                        <input type="email" id="email" name="email"  class="tooltipped " data-position="right" data-delay="50" data-tooltip="Insert valid Email" value="<?php echo($obj_user->email)?>"/>
                        <label for="email">Email</label>
                        <span id="email_error">
                            <?php
                if(isset($errors['email'])){
                    echo($errors['email']);
                }
                    
                ?> 
                        </span>
                    </div>
                </div> 
                     <div class="row">
                    <div class="input-field ">
                        <input type="text" id="contact" name="contact" class=" tooltipped" data-position="right"data-delay="50" data-tooltip="Insert Yor Contact"  value="<?php echo($obj_user->contact)?>"/>
                                          <label for="contact">Contact</label>
                                          <span id="contact_error">
                                              <?php
                if(isset($errors['contact'])){
                    echo($errors['contact']);
                }
                    
                ?> 
                                          </span>

                    </div>
                </div>
                    <div class=" row">
                        
                        <div class="input-field">
                            <button type="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
</div>