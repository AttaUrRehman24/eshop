<footer class="page_footer">
<!--*********************************************PAGE SHIPPING BAR************************************************************-->                                          
                           <div class="bar_shipping row ">
                                    <!--CASH ON DELIVERY STARTS -->
                           <section class="cash_on_delivery col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        
                           <h5 class="h3"><span class="glyphicon glyphicon-credit-card"></span>CASH ON DELIVERY</h5>
                           <p class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">We offer to pay cash on delivery for a WORRY-FREE Shopping Experience</p>
                           </section>
                                    <!--CASH ON DELIVERY ENDS -->
                                    <!--FAST SHIPPING STARTS -->
                           <section class="fast_shipping col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <h5 class="h3"><span class="fa fa-truck"></span>FAST SHIPPING</h5>
                           <p class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">Fast Shipping with doorstep Delivery all over Pakistan</p>
                           </section>
                                    <!--FAST SHIPPING ENDS -->
                                    <!--REPLACE WARRANTY STARTS -->
                           <section class="replace_warranty col-lg-4 col-md-4 col-sm-4 col-xs-4">
                           <h5 class="h3"><span class="fa fa-shield"></span>7 DAYS REPLACE WARRANTY</h5>
                           <p class="col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">FREE 7-Day replacement policy on all-products from the day of delivery (Applicable in Pakistan only</p>
                           </section>
                                    <!--REPLACE WARRANTY ENDS -->

                           </div>

                                      
                                     
                                        
<!--*********************************************PAGE MAP************************************************************-->                           
<div class="map">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCPWL9C-hTsXgrV9fapuj1Y9y3s603D14s'></script>
            <div style='overflow:hidden;height:296px;width:100%;'>
                <div id='gmap_canvas' style='height:296px;width:100%;'>
                    
                </div>
                <style>
                    #gmap_canvas img{max-width:100%!important;background:none!important}
                </style>
            </div> 
            <a href='https://mapswebsite.net/'>google maps embed code</a> 
            <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=48f6c8b9ebb944d9e1807e4817637003beb3166b'>
            </script>
            <script type='text/javascript'>function init_map(){var myOptions = {zoom:12,center:new google.maps.LatLng(31.55460609999999,74.35715809999999),mapTypeId: google.maps.MapTypeId.HYBRID};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(31.55460609999999,74.35715809999999)});infowindow = new google.maps.InfoWindow({content:'<strong>e shop</strong><br>Pace  Office #22-Link Road ,Lahore<br>54000 Lahore<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>
        </div>
    </div>  
</div>
                                     
                                    <!--INFORMATION STARTS -->
                               <div class="row">
                               <section class="information col-lg-2 col-md-2 col-sm-6 col-xs-12">
                               <h4 class="h4 "><span class="glyphicon glyphicon-info-sign"></span>"Information</h4>
                               <ul class="nav nav-pills nav-stacked">
                              <li><?php
                               echo "<a href='".BASE_URL."about.php'>About Us</a>";
                               ?></li>
                               
                              <li><?php
                               echo "<a href='".BASE_URL."privacy.php'>Privacy Policy</a>";
                               ?></li>
                              <li><?php
                               echo "<a href='".BASE_URL."terms.php'>Terms and Condition</a>";
                               ?></li>
                               </ul>
                               </section>
                                    <!--INFORMATION ENDS -->
                                    <!--EXTRA STARTS -->
                               <section class="extra col-lg-2 col-md-2 col-sm-6 col-xs-12">
                               <h4 class="h4"><span class="glyphicon glyphicon-gift"></span>Extra</h4>
                               <ul class="nav nav-pills nav-stacked">
                              <li><?php
                               echo "<a href='".BASE_URL."wishlist.php'>Wishlist</a>";
                               ?></li>
                                            
                               </ul>
                               </section>
                                    <!--EXTRA ENDS -->
                                    <!--MY ACCOUNT STARTS -->
                               <section class="my_account col-lg-2 col-md-2 col-sm-12 col-xs-12">
                               <h4 class="h4"><span class="glyphicon glyphicon-user"></span>My Account</h4>
                               <ul class="nav nav-pills nav-stacked">
                               <li><?php
                               echo "<a href='".BASE_URL."update_user.php'>Update Account</a>";
                               ?></li>
                                <li><?php
                               echo "<a href='".BASE_URL."myaccount.php'>My Account</a>";
                               ?></li>
                               
                               <li><?php
                               echo "<a href='".BASE_URL."wishlist.php'>Wishlist</a>";
                               ?></li>
                               
                               <?php
                               if($obj_user->login){
                               ?>
                               <li><?php echo "<a href='".BASE_URL."update_profile_image.php'>Update Image</a>";?></li>         
                              <?php }?>
                               </ul>
                               </section>
                                    <!--MY ACCOUNT ENDS -->
                                    <!--ABOUT US SHORT NOTE START -->
                               <div class="footer_about_us col-lg-3 col-md-3 col-sm-6 col-xs-12">
                               <h4 class="h4"><span class="fa fa-building-o"></span>About Us</h4>
                               <p class="text-muted">eshop.pk feels very proud to become your Pocket Partner.
                                                     Our aim is to provide you the best value products and deals that
                                                     you will not find anywhere else.

                                                     We are working with a team of committed professionals 
                                                     to provide you with the best services all over the Pakistan.</p>
                               </div>
                                    <!--ABOUT US SHORT NOTE ENDS -->
                                    <!--ADDRESS OF HEAD OFFICE STARTS -->
                               <section class="address_office col-lg-3 col-md-3 col-sm-6 col-xs-12"">
                               <img src="images/logo.png" class="brand-logo" alt="eshop"/>
                               <address class="address_office">
                                MAIN OFFICE<br>
                               Shop# 63,64, Zainab-Tower Link-Road Karachi<br>
                               +92 (304) 1495399<br>
                               +92 (312) 8723224<br>
                               info@eshop.pk   
                               </address>
                               </section>
                               </div>
                                    <!--ADDRESS OF HEAD OFFICE ENDS -->

                                        
                                    <!--COPY RIGHTS STARTS -->
                               <div class="copy_right navbar-inverse">
                               <p class="text-left" style="color: white;">&copy;2016.www.eshop.pk|POWERED BY:ATTA UR REHMAN</p>  
                               </div>
                                    <!--COPY RIGHT ENDS -->
   
                                        
                                    
                                        
                               </footer>

<script type="text/javascript" src="<?php echo(BASE_URL); ?>js/slider.js"></script>
<script type="text/javascript" src="<?php echo(BASE_URL); ?>js/slider2.js"></script>