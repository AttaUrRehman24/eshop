<?php

session_start();
define("BASE_URL", "http://" . $_SERVER['HTTP_HOST'] . "/" . "eshop/");
define("ITEM_PER_PAGE", 8);
if (isset($_SESSION['obj_user'])) {
    $obj_user = unserialize($_SESSION['obj_user']);
    
} else {
    $obj_user = new User();
}
if (isset($_SESSION['obj_cart'])) {
    $obj_cart = unserialize($_SESSION['obj_cart']);
} else {
    $obj_cart = new Cart();
}
if (isset($_SESSION['obj_checkout'])) {
    $obj_checkout = unserialize($_SESSION['obj_checkout']);
} else {
    $obj_checkout = new Checkout();
}



?>


<html>
    <head>
        <meta charset="UTF-8">
      

        <link rel="icon" type="image/gif" href="<?php echo(BASE_URL); ?>images/favicon.gif"/>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo(BASE_URL); ?>css/slider.css" />
       <link href="<?php echo(BASE_URL); ?>css/bootstrap.css" rel="stylesheet" type="text/css"/>
       <link href="<?php echo(BASE_URL); ?>css/materialize.css" rel="stylesheet" type="text/css"/>
        
        <link href="<?php echo(BASE_URL); ?>css/style.css" rel="stylesheet" type="text/css"/>
   <?php require_once"translate/js.php";?>
    
<script src="<?php echo(BASE_URL); ?>js/materialize.js" type="text/javascript"></script>
<script src="<?php echo(BASE_URL); ?>js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo(BASE_URL); ?>js/jquery-3.1.1.min.js" type="text/javascript"></script>

<script>
$(window).scroll(function(){
console.log($(window).scrollTop());
if($(window).scrollTop()>=200)
                {
                    if ($(window).width() >768){
		$(".collapse").css({"position":"fixed","margin-top":"-206px","z-index":"10","border-bottom":"4px solid #03a9f4","padding-top":"30px","background-color":"white","width":"100%"});
		
                
                }
            
                else
                {
                    $(".navbar-collapse").css({"position":"fixed","margin-top":"-250px","z-index":"2","background-color":"white","width":"100%"});
                $(".navbar").css({"position":"fixed","margin-top":"-300px","z-index":"2","background-color":"white","width":"100%"});
            }
            }
            else{
                $(".collapse").css({"position":"relative","margin-top":"-50px","border-bottom":"none","padding-bottom":"20px","padding-top":"45px","z-index":"1","background-color":"transparent"});
            $(".navbar").css({"position":"relative","margin-top":"auto","z-index":"1","background-color":"transparent"});
        }   
        });
        </script>
<script type="text/javascript" src="<?php echo(BASE_URL); ?>js/slider.js"></script>
<script type="text/javascript" src="<?php echo(BASE_URL); ?>js/slider2.js"></script>