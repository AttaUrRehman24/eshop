                                       
                                        <!--SECTION OF DEALS STARTS -->
                                        
                           <div class="deals row">
                           
                               <!--LATEST DEALS START -->
                                       
                           <article class="latest_deals col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                           <div class="tabs" style="padding-left:10px;">
                           <h5>Latest Deals</h5>
                           </div>
                           <div class="divider"></div>
                           <div class="card">
                           <div class="card-image waves-effect waves-block waves-light">
                           <img class="activator img-responsive" src="images/products/iphone5.png">
                           </div>
                           <div class="card-content">
                           <span class="card-title activator grey-text text-darken-4">I-Phone5<i class="material-icons right fa fa-bars"></i></span>
        
                            <p>
                            <a href="#" ><button class="btn-floating fa fa-cart-arrow-down btn-sm  "></button></a><img src="images/icons/rating_star.png" alt="rating"/>
                            <div class="divider"></div>
                            <a href="#"><button class="btn-success btn-flat ">20000 &#8360</button></a><strike >22000&#8360</strike></p>
                                 
                           </div>
                           <div class="card-reveal">
                           <span class="card-title grey-text text-darken-5 h4">Description<i class="material-icons fa fa-times right "></i></span>
                           <div class="divider"></div>
                           <p class="text-muted ">Here is some more information about this product that is only revealed once clicked on.</p>
                           </div>
                           </div>
                           </article>
                                       
                               <!--LATEST DEALS ENDS -->
                               
                               <!--NEW ARRIVALS STARTS -->
                                      
                           <section  class="  col-lg-9 col-md-9 col-sm-12 col-xs-12">
                           <div class="row">
                           <div class="col s12">
       
                           <ul class="tabs"style="overflow: hidden;">
                           <li class="tab col s3 "><a  class="active" href="#new_arrival"><h5>New Arrival</h5></a></li>
                           <li class="tab col s3 "><a  href="#special"><h5>Special</h5></a></li>
                           </ul>
    
                           <div class="divider"></div>
                            </div>                                      
                           <div id="new_arrival" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 " style="margin: 0px; padding: 0px;">
                           <?php
                           try {
                           $products=  Product::get_index_product($action="new_arrival");
                           foreach ($products as $p)
                               {
                               
                           
                           ?>
                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="card">
                                               <div class="card-image waves-effect waves-block waves-light">
                         
                          <img class="img-responsive activator"  src="<?php echo(BASE_URL);?>Admin/products/<?php echo("$p->product_name/$p->product_image");?>" alt="<?php echo($p->product_name);?>">
                       
                           </div>
                           <div class="card-content">
                           <span class="card-title activator grey-text text-darken-4"><?php echo($p->product_name);?><i class="material-icons right fa fa-bars"></i></span>
        
                            <p>
                                <form action="process/process_cart.php" method="post">
                                    <input type="hidden" name="action" value="add_to_cart">
                                                    <input type="hidden" name="productID" value="<?php echo($p->productID); ?>">
                                                    
                                                    <button  type="submit" class="btn-floating fa fa-cart-arrow-down btn-sm  "></button><img src="images/icons/rating_star.png" alt="rating"/>
                                </form>
                                                    <div class="divider"></div>
                            <a href="<?php echo(BASE_URL);?>/product_detail.php?productID=<?php echo($p->productID);?>"><button class="btn-success btn-flat "><?php echo($p->product_price);?>&#8360</button></a><strike class="right center"><?php echo($p->product_discount);?>&#8360</strike></p>
                                 
                           </div>
                                               <div class="card-reveal" style="z-index: 1;">
                           <span class="card-title grey-text text-darken-5 h4">Description<i class="material-icons fa fa-times right "></i></span>
                           <div class="divider"></div>
                           <p class="text-muted text-info text-capitalize"><?php echo($p->proto_description); ?></p>
                           </div>
                           </div>
                            </div>
                               <?php
                               }   
                               } catch (Exception $ex) {
                                   echo $ex->getMessage();   
                               }
                               ?>
                         
        
                           </section>
                                       
                                              
                                        <!--NEW ARRIVALS ENDS -->
                          
                                        <!--SOME SPECIAL STARTS -->
                                        
<?php
require_once ("views/product.php"); 
?>
                                        <!--SOME SPECIAL ENDS -->
                           </div>
                                        <!--SECTION OF DEALS ENDS -->
                                  
                                        
                                        <!--SECTION OF FASHION AND CLOTHING STARTS -->
                           <div class="section-of-fashion row">
                                        <!--LEFT ASIDE OF FASHION AND CLOTHING STARTS -->

                           <article class="fashion_and_clothing">
                           <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 panel-default">
                           <div class="panel-heading">
                            Fashion & Clothing
                           </div>
                           <div class="panel-body">
                           <ul class="nav nav-pills nav-stacked">
                            <?php
                           try {
                              $categorys= Category::get_category();
                                foreach ($categorys as $cat){
                                    if($cat>=$categorys[9] && $cat<=$categorys[11])
                                    {
                                     
                                   echo("<li><a href='" . BASE_URL . "product_view.php?categoryID=$cat->categoryID'>$cat->category_name</a></li>");
                                        
                                        }
                                
                                    }    
                                                                } catch (Exception $ex) {
                                                                 echo"<li>'".$ex->getMessage()."'</li>";   
                                                                }
                                ?>
                           </ul>
                           </div>
        
                           </div>                                          

        
                           </article>
                                        <!--LEFT ASIDE OF FASHION AND CLOTHING ENDS -->
                                        <!--RIGHT MINI SLIDER OF FASHION AND CLOTHING STARTS -->
                                      
                           <section class="fashion_and_clothing_slider">
                           <div class="col-lg-9 col-md-9 col-sm-12 hidden-xs">
                           <?php
                           try {
                           $products=  Product::get_index_product($action="fashion");
                           foreach ($products as $p)
                               {
                               
                           
                           ?>
                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="card">
                                               <div class="card-image waves-effect waves-block waves-light">
                         
                          <img class="img-responsive activator"  src="<?php echo(BASE_URL);?>Admin/products/<?php echo("$p->product_name/$p->product_image");?>" alt="<?php echo($p->product_name);?>">
                       
                           </div>
                           <div class="card-content">
                           <span class="card-title activator grey-text text-darken-4"><?php echo($p->product_name);?><i class="material-icons right fa fa-bars"></i></span>
        
                            <p>
                                <form action="process/process_cart.php" method="post">
                                    <input type="hidden" name="action" value="add_to_cart">
                                                    <input type="hidden" name="productID" value="<?php echo($p->productID); ?>">
                                                    
                                                    <button  type="submit" class="btn-floating fa fa-cart-arrow-down btn-sm  "></button><img src="images/icons/rating_star.png" alt="rating"/>
                                </form>
                                                    <div class="divider"></div>
                            <a href="<?php echo(BASE_URL);?>/product_detail.php?productID=<?php echo($p->productID);?>"><button class="btn-success btn-flat "><?php echo($p->product_price);?>&#8360</button></a><strike class="right center"><?php echo($p->product_discount);?>&#8360</strike></p>
                                 
                           </div>
                                               <div class="card-reveal" style="z-index: 1;">
                           <span class="card-title grey-text text-darken-5 h4">Description<i class="material-icons fa fa-times right "></i></span>
                           <div class="divider"></div>
                           <p class="text-muted text-info text-capitalize"><?php echo($p->proto_description); ?></p>
                           </div>
                           </div>
                            </div>
                               <?php
                               }   
                               } catch (Exception $ex) {
                                   echo $ex->getMessage();   
                               }
                               ?>
                           </div>
                                            
                           </section>
                                        <!--RIGHT MINI SLIDER OF FASHION AND CLOTHING ENDS -->
                           </div>
                                    <!--SECTION OF FASHION AND CLOTHING ENDS -->
                                 
                                        
                                        <!--SECTION OF ELECTRONIC STARTS -->
                           <div class="section-of-electronics  row">
                                        <!--LEFT ASIDE OF ELECTRONIC  STARTS-->
                           <article class="electronis">
                           <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 panel-default">
                           <div class="panel-heading">
                           ELECTRONICS
                           </div>
                           <div class="panel-body">
                           <ul class="nav nav-pills nav-stacked">
                            <?php
                                try {
                                $categorys=  Category::get_category();
                             
                                foreach($categorys as $cat ){
                                 if($cat<=$categorys[2]){
                                echo("<li><a href='" . BASE_URL . "product_view.php?categoryID=$cat->categoryID'>$cat->category_name</a></li>");
                                 }
                                }

                                     
                                    
                                }
                                 catch (Exception $ex) {
                                    echo "<li>".$ex->getMessage()."</li>";
                                }
                                
                                ?>
                           </ul>
                           </div>
        
                           </div>   
                           </article>
                            
                                        <!--LEFT ASIDE OF ELECTRONIC ENDS -->
                                        
                                        <!--RIGHT MINI SLIDER OF ELECTRONIC STARTS -->
                                        
                           <section class="electronic_slider">
                           <div class="col-lg-9 col-md-9 col-sm-12 hidden-xs">
                           <?php
                           try {
                           $products=  Product::get_index_product($action="electronic");
                           foreach ($products as $p)
                               {
                               
                           
                           ?>
                               <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                             <div class="card">
                                               <div class="card-image waves-effect waves-block waves-light">
                         
                          <img class="img-responsive activator"  src="<?php echo(BASE_URL);?>Admin/products/<?php echo("$p->product_name/$p->product_image");?>" alt="<?php echo($p->product_name);?>">
                       
                           </div>
                           <div class="card-content">
                           <span class="card-title activator grey-text text-darken-4"><?php echo($p->product_name);?><i class="material-icons right fa fa-bars"></i></span>
        
                            <p>
                                <form action="process/process_cart.php" method="post">
                                    <input type="hidden" name="action" value="add_to_cart">
                                                    <input type="hidden" name="productID" value="<?php echo($p->productID); ?>">
                                                    
                                                    <button  type="submit" class="btn-floating fa fa-cart-arrow-down btn-sm  "></button><img src="images/icons/rating_star.png" alt="rating"/>
                                </form>
                                                    <div class="divider"></div>
                            <a href="<?php echo(BASE_URL);?>/product_detail.php?productID=<?php echo($p->productID);?>"><button class="btn-success btn-flat "><?php echo($p->product_price);?>&#8360</button></a><strike class="right center"><?php echo($p->product_discount);?>&#8360</strike></p>
                                 
                           </div>
                                               <div class="card-reveal" style="z-index: 1;">
                           <span class="card-title grey-text text-darken-5 h4">Description<i class="material-icons fa fa-times right "></i></span>
                           <div class="divider"></div>
                           <p class="text-muted text-info text-capitalize"><?php echo($p->proto_description); ?></p>
                           </div>
                           </div>
                            </div>
                               <?php
                               }   
                               } catch (Exception $ex) {
                                   echo $ex->getMessage();   
                               }
                               ?>
                           </div>
                                            
                           </section>
                                        <!--RIGHT MINI SLIDER OF CLEARANCE SALE ENDS -->
                           </div>
                                        <!--SECTION OF CLEARANCE SALE ENDS -->