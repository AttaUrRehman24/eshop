<?php
 require_once ("models/user.php");
 require_once 'models/category.php';
 require_once 'models/brand.php';
 require_once 'models/checkout.php';
 require_once 'models/product.php';
 require_once 'models/cart.php';
 require_once ("views/top.php")?>
 <?php
           if($obj_user->login){
               echo "<title> Wishlist-$obj_user->user_name</title>";

           }
            else {
                   echo "<title>Wishlist- Guest</title>";
 }
           
           ?>
    </head>
    <body>
        <?php
       
       require_once ("views/header_top.php");
       require_once ("views/middle_header.php");
       require_once ("views/bootom_header.php");
       ?>
        <div class="page_body">
           <?php
//           if($obj_user->login){
               echo "<h3 class='center'>Your Cart</h3>";
               ?>
            <form action="process/process_cart.php" method="post">
            <table class="table table-hover table-responsive">
                <thead>
                    <tr>
                        <th>
                            Product Name
                        </th>
                       
                        <th>
                            Product Quantity
                        </th>
                        <th>
                            Product Price
                        </th>
                        <th>
                            View Detail
                        </th>
                        <th>
                           
                             Total Price
                        </th>
                        <th>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($obj_cart->items as $item){
                        
                       echo "<tr>";
                                   echo "<th>";
                                   echo"$item->item_name";
                                   echo "</th>";
                                       echo "<th>";
                                   echo "$item->quantity";
                                   echo "</th>";
                                   echo "<th>";
                                   echo "$item->product_price";
                                   echo "</th>";
                                   echo "<th>";
                                   echo " <a class='btn btn-info' role='button' href='".BASE_URL."product_detail.php?productID=$item->itemID'>View Detail</a>";
                                   echo "</th>";
                                   echo "<th>";
                                   echo "$obj_cart->total_price";
                                   echo "</th>";
                                   echo "<th><a href='".BASE_URL."/process/process_cart.php?action=remove_item&productID=$item->itemID'>Delete</a></th>";
                                   echo "</tr>";
                                   echo "<br>";
                                   echo "<br>";
                                   
                                
                    }
                    ?>
                    
               
                </tbody>
            </table>
                <a class="btn btn-info waves-effect waves-light" role="button" href="<?php echo (BASE_URL);?>checkout.php" >CheckOut</a>
                </form>
           <?php
//           }
//            else {
//                   echo "<h3 class='center'> For Purchase </h3>";
//                  echo"<br><br>";
//                   echo"<h4 class='center'> Go Login <a href='".BASE_URL."/login.php'>Login</a></h4>";
// echo"<br><br>";
//                   echo"<h4 class='center'> Go Login <a href='".BASE_URL."/signup.php'>Signup</a></h4>";
//            } 
 
           
           ?>
        </div>
        <div class="footer ">
        <?php
       require_once ("views/footer.php");
        ?>
        </div>
        <!--FOOTER ENDS -->
 