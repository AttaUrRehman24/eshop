
<?php
 require_once ("models/user.php");
 require_once 'models/category.php';
 require_once 'models/brand.php';
 require_once 'models/checkout.php';
 require_once 'models/product.php';
 require_once 'models/cart.php';
 require_once ("views/top.php");
?>

<?php
if ($obj_user->login) {
    echo('<title>' . $obj_user->user_name . ' || My Account</title>');
} else {
    echo("<title>Home Shop || My Account</title>");
}
?> 


<link href="style/ProfileCss.css" rel="stylesheet" type="text/css">
</head>
<body>
    <?php
//    echo("<pre>");
//    print_r($_SESSION['obj_user']);
//    echo("</pre>");
//
//    die;
    ?>
    <div class="wrap">
        <div class="header">
            <?php
           require_once ("views/header_top.php");
       require_once ("views/middle_header.php");
       require_once ("views/bootom_header.php");
            ?>
        </div>
        <div class="main">

            <?php
            
                ?>

                <div class="content">
                    <div id="heading-row"><h3>Update User Profile</h3>
                        <?php
                         if(isset($_SESSION['msg'])){
                            $msg = $_SESSION['msg'];
                          
                            echo($msg);
                         }
            if (isset($_SESSION['msg_err'])) {
                echo($_SESSION['msg_err'] . "<hr>");
                unset($_SESSION['msg_err']);
            }
             if (isset($_SESSION['errors'])) {
                            $errors = $_SESSION['errors'];
                        }
            
    
                        if (isset($_SESSION['obj_user'])) {
                            $obj_user = unserialize($_SESSION['obj_user']);
                        } else {
                            $obj_user= new User();
                            
                        }
    
                            if(isset($errors['profile_image'])){
                                echo $errors['profile_image'];
                            }
                                ?>
                       
                    </div>
           

                        <div class="cont-desc span_1_of_2">
                            <?php
                            require_once 'views/update_image.php';
                            ?>
                        </div>

                        <div class="rightsidebar span_3_of_1">
                            <?php
                            require_once 'views/myaccount_option.php';
                            ?>
                        </div>
         

    <?php

    
    require_once 'views/footer.php';

?>

</body>
</html>
