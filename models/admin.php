<?php

require_once "DB_Connection.php";

class Admin extends DB_Connection {

    private $adminID;
    private $first_name;
    private $last_name;
    private $email;
    private $admin_name;
    private $password;
    private $contact;
    private $gender;
    private $interest;
    private $date;
    private $address;
    private $city;
    private $state;
    private $country;
    private $profile_image;
    private $login_status;
    private $is_active;
    private $signup_date;
    
   public function __construct() {
        $this->interest = array();
    }
    public function __set($name, $value) {
        $method_name="set_$name";
        if(!method_exists( $this , $method_name))
        {
            throw new Exception("The set Property of $name is not found");
        }
        $this->$method_name($value);
    }
    public function __get($name) {
        $method_name="get_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("The get property of $name is not exist");
        }
        return$this->$method_name();
    }
    private function set_adminID($adminID){
        if(!is_numeric($adminID)||$adminID<=0)
        {
            throw new Exception("Admin ID incorrect");
        }
        $this->adminID=$adminID;
    }
    private function get_adminID(){
        return$this->adminID;
    }
    private function set_first_name($first_name){
        $reg = "/^[a-z]+$/i";
        if (!preg_match($reg, $first_name)) {
            throw new Exception(" * Invalid/Missing First name");
        }
        $this->first_name= ucfirst(strtolower($first_name));
    }
    private function get_first_name(){
        return$this->first_name;
    }
     private function set_last_name($last_name) {
        $reg = "/^[a-z]+$/i";
        if (!preg_match($reg, $last_name)) {
            throw new Exception(" * Invalid/Missing Last name");
        }
        $this->last_name = ucfirst(strtolower($last_name));
    }

    private function get_last_name() {
        return $this->last_name;
    }
    
       private function get_full_name() {

        if (is_null($this->first_name) || is_null($this->last_name)) {
            return "guest";
        }
        $full_name = "$this->first_name $this->last_name";
        return $full_name;
    }
       private function set_email($email) {
        $reg = "/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zAZ]\.)+[a-zA-Z]{2,4})$/";
        if (!preg_match($reg, $email)) {
            throw new Exception(" * Invalid/Missing Email");
        }
        $this->email = $email;
    }

    private function get_email() {
        return $this->email;
    }
        private function set_admin_name($admin_name) {
        $reg = "/^[a-z]+$/i";
        if (!preg_match($reg, $admin_name)) {
            throw new Exception(" * Invalid/Missing User name");
        }
        $this->admin_name = $admin_name;
    }

    private function get_admin_name() {
        return $this->admin_name;
    }
       private function set_password($password) {
        $reg = "/^[a-z|0-9]+(-|\.)?$/i";
        if (!preg_match($reg, $password)) {
            throw new Exception(" * Invalid/Missing password");
        }
        $this->password = $password;
    }

    private function get_password() {
        return $this->password;
    }
    
       private function set_contact($contact) {
        $reg = "/^\d{4}?-?\d{7}/";

        if (!preg_match($reg, $contact)) {
            throw new Exception(" * Invalid/Missing Contact Number");
        }

        $this->contact = $contact;
    }

    private function get_contact() {
        return $this->contact;
    }
    
    private function set_gender($gender) {

        $genders = array("Male", "Female","Other");
        if (!in_array($gender, $genders)) {
            throw new Exception(" * Missing Gender");
        }
        $this->gender = $gender;
    }

    private function get_gender() {
        return $this->gender;
    }
    private function set_interest($interest) {
        if(!is_array($interest)){
            throw new Exception("Missing Interset");
        }
 $iOptions = array("Football","Hockey" ,"Cricket");

        foreach ($interest as $i) {
            if (!in_array($i, $iOptions)) {
                throw new Exception(" * Invalid Interests Option(s)");
            }
        }l
                $this->interest=$interest;
            }
private function get_interest(){
    return$this->interest;
}

private function set_date($date){
    $reg= "/^\d{2}\/\d{2}\/\d{4}$/";
    if(!preg_match($reg, $date))
    {
        throw new Exception("Invalid Format");
    }
   $parts=  explode("/", $date);
   list($month,$day,$year)=$parts;
  if(!isset($day) || !isset($month) || !isset($year)){
            throw new Exception("Invalid Date of Birth");
        }
        if(!checkdate($month, $day, $year))
        {
            throw new Exception("Invalid Pattern");
        }
        $this->date = mktime(0,0,0,$month,$day,$year);
}
private function get_date() {
   if(is_null($this->date))
    {
       return 0;
    }
    $date=date("d-m-y", $this->date);
    return$date;
}
  private function set_profile_image($profile_image) {

        if ($profile_image['error'] == 4) {
            throw new Exception(" * File Missing");
        }

        $image = getimagesize($profile_image['tmp_name']);

        if (!$image) {
            throw new Exception(" * not a valid image");
        }

        if ($profile_image['size'] > 100000000000) {
            throw new Exception(" * max file size allowed is 1000 K");
        }

      

        if ($profile_image['type'] != $image['mime']) {
            throw new Exception(" * Corrupt image");
        }

        if (is_null($this->admin_name)) {
            throw new Exception(" * Failed to generate file name");
        }

        $path_info = pathinfo($profile_image['name']);
        extract($path_info);

        $this->profile_image = $this->admin_name . "." . $extension;
    }

    private function get_profile_image() {
        return $this->profile_image;
    }



    public function upload_profile_image($soruce_path) {
        $str_path = "../admins/$this->admin_name/$this->profile_image";
        if (!is_dir("../admins")) {
            if (!mkdir("../admins")) {
                throw new Exception(" * failed to create folder ../admins");
            }
        }

        if (!is_dir("../admins/$this->admin_name")) {
            if (!mkdir("../admins/$this->admin_name")) {
                throw new Exception(" * failed to create folder ../admins/$this->admin_name");
            }
        }

        $result = @move_uploaded_file($soruce_path, $str_path);

        if (!$result) {
            throw new Exception(" * Faield to uplaod file");
        }
    }

private function set_state($state){
    if(empty($state))
    {
        throw new Exception("Missing State");
    }
    $this->state=$state;
}
private function get_state() {
        return $this->state;
    }
    private function set_city($city){
    if(empty($city))
    {
        throw new Exception("Missing City");
    }
    $this->city=$city;
}
private function get_city() {
        return $this->city;
    }
   private function set_address($address){
        if(empty($address))
        {
            throw new Exception("Missing Street Address");
        }
        if(!strlen($address<20))
        {
            throw new Exception("Too Short Address");
        }
        $this->address=$address;
    }
    private function get_address(){
        return$this->address;
    }
    private function set_country($country) {
        $c_list=array("Afghanistan",
	"Albania",
	"Algeria",
	"American Samoa",
	"Andorra",
	"Angola",
	"Anguilla",
	"Antarctica",
	"Antigua and Barbuda",
	"Argentina",
	"Armenia",
	"Aruba",
	"Australia",
	"Austria",
	"Azerbaijan",
	"Bahamas",
	"Bahrain",
	"Bangladesh",
	"Barbados",
	"Belarus",
	"Belgium",
	"Belize",
	"Benin",
	"Bermuda",
	"Bhutan",
	"Bolivia",
	"Bosnia and Herzegovina",
	"Botswana",
	"Bouvet Island",
	"Brazil",
	"British Indian Ocean Territory",
	"Brunei Darussalam",
	"Bulgaria",
	"Burkina Faso",
	"Burundi",
	"Cambodia",
	"Cameroon",
	"Canada",
	"Cape Verde",
	"Cayman Islands",
	"Central African Republic",
	"Chad",
	"Chile",
	"China",
	"Christmas Island",
	"Cocos (Keeling) Islands",
	"Colombia",
	"Comoros",
	"Congo",
	"Congo, the Democratic Republic of the",
	"Cook Islands",
	"Costa Rica",
	"Cote D'Ivoire",
	"Croatia",
	"Cuba",
	"Cyprus",
	"Czech Republic",
	"Denmark",
	"Djibouti",
	"Dominica",
	"Dominican Republic",
	"Ecuador",
	"Egypt",
	"El Salvador",
	"Equatorial Guinea",
	"Eritrea",
	"Estonia",
	"Ethiopia",
	"Falkland Islands (Malvinas)",
	"Faroe Islands",
	"Fiji",
	"Finland",
	"France",
	"French Guiana",
	"French Polynesia",
	"French Southern Territories",
	"Gabon",
	"Gambia",
	"Georgia",
	"Germany",
	"Ghana",
	"Gibraltar",
	"Greece",
	"Greenland",
	"Grenada",
	"Guadeloupe",
	"Guam",
	"Guatemala",
	"Guinea",
	"Guinea-Bissau",
	"Guyana",
	"Haiti",
	"Heard Island and Mcdonald Islands",
	"Holy See (Vatican City State)",
	"Honduras",
	"Hong Kong",
	"Hungary",
	"Iceland",
	"India",
	"Indonesia",
	"Iran, Islamic Republic of",
	"Iraq",
	"Ireland",
	"Israel",
	"Italy",
	"Jamaica",
	"Japan",
	"Jordan",
	"Kazakhstan",
	"Kenya",
	"Kiribati",
	"Korea, Democratic People's Republic of",
	"Korea, Republic of",
	"Kuwait",
	"Kyrgyzstan",
	"Lao People's Democratic Republic",
	"Latvia",
	"Lebanon",
	"Lesotho",
	"Liberia",
	"Libyan Arab Jamahiriya",
	"Liechtenstein",
	"Lithuania",
	"Luxembourg",
	"Macao",
	"Macedonia, the Former Yugoslav Republic of",
	"Madagascar",
	"Malawi",
	"Malaysia",
	"Maldives",
	"Mali",
	"Malta",
	"Marshall Islands",
	"Martinique",
	"Mauritania",
	"Mauritius",
	"Mayotte",
	"Mexico",
	"Micronesia, Federated States of",
	"Moldova, Republic of",
	"Monaco",
	"Mongolia",
	"Montserrat",
	"Morocco",
	"Mozambique",
	"Myanmar",
	"Namibia",
	"Nauru",
	"Nepal",
	"Netherlands",
	"Netherlands Antilles",
	"New Caledonia",
	"New Zealand",
	"Nicaragua",
	"Niger",
	"Nigeria",
	"Niue",
	"Norfolk Island",
	"Northern Mariana Islands",
	"Norway",
	"Oman",
	"Pakistan",
	"Palau",
	"Palestinian Territory, Occupied",
	"Panama",
	"Papua New Guinea",
	"Paraguay",
	"Peru",
	"Philippines",
	"Pitcairn",
	"Poland",
	"Portugal",
	"Puerto Rico",
	"Qatar",
	"Reunion",
	"Romania",
	"Russian Federation",
	"Rwanda",
	"Saint Helena",
	"Saint Kitts and Nevis",
	"Saint Lucia",
	"Saint Pierre and Miquelon",
	"Saint Vincent and the Grenadines",
	"Samoa",
	"San Marino",
	"Sao Tome and Principe",
	"Saudi Arabia",
	"Senegal",
	"Serbia and Montenegro",
	"Seychelles",
	"Sierra Leone",
	"Singapore",
	"Slovakia",
	"Slovenia",
	"Solomon Islands",
	"Somalia",
	"South Africa",
	"South Georgia and the South Sandwich Islands",
	"Spain",
	"Sri Lanka",
	"Sudan",
	"Suriname",
	"Svalbard and Jan Mayen",
	"Swaziland",
	"Sweden",
	"Switzerland",
	"Syrian Arab Republic",
	"Taiwan, Province of China",
	"Tajikistan",
	"Tanzania, United Republic of",
	"Thailand",
	"Timor-Leste",
	"Togo",
	"Tokelau",
	"Tonga",
	"Trinidad and Tobago",
	"Tunisia",
	"Turkey",
	"Turkmenistan",
	"Turks and Caicos Islands",
	"Tuvalu",
	"Uganda",
	"Ukraine",
	"United Arab Emirates",
	"United Kingdom",
	"United States",
	"United States Minor Outlying Islands",
	"Uruguay",
	"Uzbekistan",
	"Vanuatu",
	"Venezuela",
	"Viet Nam",
	"Virgin Islands, British",
	"Virgin Islands, US",
	"Wallis and Futuna",
	"Western Sahara",
	"Yemen",
	"Zambia",
	"Zimbabwe");
        if(!in_array($country, $c_list)){
            throw new Exception("Invalid Choice");
        }
        $this->country=$country;
    }

    private function get_country() {
        return $this->country;
        
    }

    public function add_admin($act_code)
    {
        $obj_db= $this->obj_db();
        $query="INSERT INTO `admins` "
                . "(`adminID`, `first_name`, `last_name`, "
                . "`email`, `admin_name`, `password`, `gender`, `interest`, "
                . "`contact`, `date_of_birth`, `address`, "
                . "`city`, `state`, `country`, `profile_image`, "
                . "`reset_code`) "
                . "VALUES"
                . "('$this->adminID', '$this->first_name', "
                . "'$this->last_name', "
                . "'$this->email', '$this->admin_name', '$this->password', "
                . "'$this->gender', '" . serialize($this->interest) . "', "
                . "'$this->contact', '$this->date', "
                . "'$this->address', '$this->city', '$this->state', "
                . "'$this->country', '$this->profile_image', "
                . "'$act_code');";
        $result=$obj_db->query($query);
        if ($obj_db->errno) {
            throw new Exception(" * New User Insert Error - $obj_db->error -$obj_db->errno");
        }
        if ($obj_db->errno == 1062) {
            throw new Exception("user alredy exist");
}

        $this->adminID = $obj_db->insert_id;
        
    }
       public function activate_admin($act_code) {


        $obj_db = $this->obj_db();

       $query_select = "SELECT adminID, is_active, reset_code,register_time,delete_state=0"
                . " FROM"
                . " users"
                . " WHERE"
                . " adminID= '$this->adminID'"
                . " AND"
                . " reset_code= '$act_code'";
    
        $result = $obj_db->query($query);

        if ($obj_db->errno) {
            throw new Exception(" * Select Activate Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception(" * Activation Data Not Found");
        }
$data=$result->fetct_object();
if($data->is_active)
{
    throw new Exception("Your account is Already activate");
}
$exp_time = strtotime($data->register_time) + (60 * 60 * 24 * 3);
$now=  time();
        if ($now > $exp_time) {
            $query_delete = "delete from `admins` "
                    . "where `adminID` = '$this->adminID'";
            $r2 = $obj_db->query($query_delete);
            if ($obj_db->affected_rows == 0) {
                throw new Exception(" * Nothing deleted");
            }
            throw new Exception(" * Your activation link has expired. Register again");
        }
       $query_update="UPDATE admins"
                . " SET"
                . " is_active= 1,"
                . " reset_code= NULL"
                . " WHERE"
                . " userID= $this->adminID";
        $r3 = $obj_db->query($query_update);
        if ($obj_db->affected_rows == 0) {
            throw new Exception(" * Nothing Update");
        }
    
        
       }
          public function login($remember = FALSE) {

        $obj_db = $this->obj_db();

        $query = "SELECT adminID,email,first_name,gender, last_name,profile_image,admin_name FROM `admins` WHERE admin_name ='$this->admin_name' AND `password` = '$this->password'";

        $result = $obj_db->query($query);
        
        if ($obj_db->errno) {
            throw new Exception(" * Select Login Error - $obj_db->error -$obj_db->errno");
        }
    if ($result->num_rows == 0) {
            throw new Exception("-Failed ");
        }
        

        $data = $result->fetch_object();
        $this->first_name=$data->first_name;
        $this->last_name=$data->last_name;
        $this->adminID = $data->adminID;
        $this->email = $data->email;
        $this->profile_image=$data->profile_image;
        $this->admin_name = $data->admin_name;
        $this->password=$data->password;
        $this->login_status = TRUE;

        $str_admin = serialize($this);
  
        $_SESSION['obj_admin'] = $str_admin;

        if ($remember) {
            $expire = time() + (60 * 60 * 24 * 15);
            setcookie("obj_admin", $str_admin, $expire, "/");
        }
    }

    public function logout() {
        if (isset($_SESSION['obj_admin'])) {
            unset($_SESSION['obj_admin']);
        }
        if (isset($_COOKIE['obj_admin'])) {
            setcookie("obj_admin", "", 1, "/");
        }
    }

    private function get_login() {
        return $this->login_status;
    }

  
    private function get_is_active() {
        return $this->is_active;
    }

      public static function get_sigle_admins($key) {

        $obj_db = self::obj_db();

        $query = "select * from `admins` where adminID='$key' ";


        $result = $obj_db->query($query);

        if ($obj_db->errno) {
            throw new Exception(" * admin selection Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception("  admin not found");
        }

        $admins = array();
        while ($data = $result->fetch_object()) {

            $temp = new Admin();
            $temp->adminID = $data->adminID;
            $temp->admin_name = $data->admin_name;
            $temp->last_name = $data->last_name;
            $temp->address = $data->address;
            $temp->first_name = $data->first_name;
            $temp->email = $data->email;
            $temp->gender = $data->gender;
            $temp->country = $data->country;
            $temp->profile_image = $data->profile_image;
            $temp->date= $data->date_of_birth;
            $temp->contact = $data->contact;
            $temp->is_active = $data->is_active;

            $admins[] = $temp;
        }

        return $admins;
    }
    public static function get_admins() {

        $obj_db = self::obj_db();

        $query = "select * from `admins` ";


        $result = $obj_db->query($query);

        if ($obj_db->errno) {
            throw new Exception(" * admin selection Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception(" * admin (s) not found");
        }

        $admins = array();
        while ($data = $result->fetch_object()) {

            $temp = new Admin();
            $temp->adminID = $data->adminID;
            $temp->admin_name = $data->admin_name;
            $temp->last_name = $data->last_name;
            
            $temp->first_name = $data->first_name;
            $temp->email = $data->email;
            $temp->gender = $data->gender;
            $temp->country = $data->country;
            $temp->profile_image = $data->profile_image;
            $temp->date= $data->date_of_birth;
            $temp->contact = $data->contact;
            $temp->is_active = $data->is_active;

            $admins[] = $temp;
        }

        return $admins;
    }
    
    public function delete_admin($key) {

        $obj_db = $this->obj_db();

        $query = "delete from `admins` "
                . "where adminID = '$key' ";


        $result = $obj_db->query($query);

        if ($obj_db->affected_rows == 0) {
            throw new Exception(" * Nothing delete");
        }
    }
    public function update_admin($key){
        
        $obj_db= $this->obj_db();
        $query="update admins set first_name='".$_POST['first_name']."',last_name='".$_POST['last_name']."',email='".$_POST['email']."',contact='".$_POST['contact']."' where adminID=$key";
        $result=$obj_db->query($query);
   
        if($obj_db->error)
        {
            throw new Exception("No Admin Found $obj_db->errno->>>$obj_db->error");
        }
        if($result->num_rows){
            throw new Exception("NO Admin Updated");
        }
        
    }
}