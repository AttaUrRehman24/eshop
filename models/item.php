<?php
require_once 'DB_Connection.php';
class Item extends DB_Connection{
    
    
    private $itemID;
    private $quantity;
   
    
    public function __construct($itemID , $quantity=1) {
          $this->set_itemID($itemID);
        $this->set_quantity($quantity);
    }
  public function __set($name, $value) {
        $restircted = array("itemID");
        
        if(in_array($name, $restircted)){
            throw new Exception("Property $name is read-only");
        }
        $method_name = "set_$name";

        if (!method_exists($this, $method_name)) {
            throw new Exception("SET Property $name does not exist");
        }

        $this->$method_name($value);
    }
      public function __get($name) {
        $method_name = "get_$name";

        if (!method_exists($this, $method_name)) {
            throw new Exception("GET Property $name does not exist");
        }

        return $this->$method_name();
    }
    private function set_itemID($itemID){
        if(!is_numeric($itemID)&& $itemID<=0){
            throw new Exception("Invalid Item ");   
        }
        $this->itemID=$itemID;
    }
    private function get_itemID(){
        return $this->itemID;
    }
     private function set_quantity($quantity){
        if(!is_numeric($quantity) && $quantity <= 0){
            throw new Exception("Invalid Quantity");
        }
        $this->quantity = $quantity;
        
    }

    private function get_quantity(){
        return $this->quantity;
    }
    private function get_item_name(){
        $obj_db= $this->obj_db();
        $query="SELECT product_name FROM products WHERE productID='$this->itemID'";
        $result=$obj_db->query($query);
        
        $data=$result->fetch_object();
        return $data->product_name;
    }
    private function  get_product_price(){
        $obj_db=  $this->obj_db();
        $query="SELECT product_price FROM products WHERE productID='$this->itemID'";
         $result=$obj_db->query($query);
         
        $data=$result->fetch_object();
        
        return $data->product_price;
      
   }
   private function get_total_price(){
        $total=  $this->product_price * $this->quantity;
        return$total;
    }
    
    
}