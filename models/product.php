<?php
require_once 'DB_Connection.php';

require_once 'category.php';
require_once 'brand.php';

class Product extends DB_Connection{
    private $productID;
    private $product_name;
    private $product_discount;
    private $product_price;
    private $description;
    private $proto_description;
    private $product_image;
    private $view_count;
    private $brand;
    private $category;
    private $quantity;
    
    public function __construct() {
   
    }
    public function __set($name, $value) {
        $method_name = "set_$name";

        if (!method_exists($this, $method_name)) {
            throw new Exception("The set Property of $name is not exist");
        }

        $this->$method_name($value);
    }

    public function __get($name) {
        $method_name = "get_$name";

        if (!method_exists($this, $method_name)) {
            throw new Exception(" The get Property $name is not exist");
        }

        return $this->$method_name();
    }
    private function set_productID($productID){
        if(!is_numeric($productID)||$productID<=0)
        {
            throw new Exception("Invalid Product ID");
        }
        $this->productID=$productID;
        
    }
    private function get_productID(){
        return $this->productID;
    }
        private function set_product_name($product_name) {
        $reg = "/^[a-z\s||0-9]+$/i";
        if (!preg_match($reg, $product_name)) {
            throw new Exception("Invalid or Missing product name");
        }
        $this->product_name =$product_name;
    }

    private function get_product_name() {
        return $this->product_name;
    }
    private function set_description($description){
        $description=  trim($description);
        if(empty($description)||  strlen($description)<=10){
            throw new Exception("Product Description is invalid or empty");
        }
        $this->description=$description;
        
    }
    private function get_description(){
        return $this->description;
    }
      private function set_product_price($product_price) {

        if (!is_numeric($product_price)) {
            throw new Exception("Invalid Or Missing Product price");
        }
        $this->product_price = $product_price;
    }

    private function get_product_price() {
        return $this->product_price;
    }
    private function set_proto_description($proto_description){
        $proto_description=  trim($proto_description);
        if(empty($proto_description)|| strlen($proto_description)<=10)
        {
            throw new Exception("It Is to Short Or Missing");
        }
        $this->proto_description=$proto_description;
    }
    private function get_proto_description(){
        return $this->proto_description;
    }

    private function set_quantity($quantity) {

        if (!is_numeric($quantity) || $quantity <= 0) {
            throw new Exception("Incalid Or Missing quantity");
        }
        $this->quantity = $quantity;
    }

    private function get_quantity() {
        return $this->quantity;
    }
    private function set_product_image($product_image){
   
        if($product_image['error']==4){
            throw new Exception("Product image is missing");
        }
        $image=  getimagesize($product_image['tmp_name']);
       
        if(!$image)
        {
            throw new Exception("Image Invalid");
        }
        if($product_image['size']>1000000000){
            throw new Exception("Image is To Large");
        }
     
        if ($product_image['type'] != $image['mime']) {
            throw new Exception(" * Corrupt image");
        }

        if (is_null($this->product_name)) {
            throw new Exception("Failed to generate file name");
        }
        $path_info=  pathinfo($product_image['name']);
        extract($path_info);
        $this->product_image=$this->product_name.".".$extension;
    }
     private function get_product_image() {
        return $this->product_image;
    }
     private function set_category($category)
    {
        $this->category=$category;
    }
    private function get_category(){
        return$this->category;
    }
     private function set_brand($brand)
    {
        
        $this->brand=$brand;
    }
    private function get_brand(){
        return $this->brand;
    }
   public function upload_product_image($soruce_path) {
        $str_path = "../products/$this->product_name/$this->product_image";
        if (!is_dir("../products")) {
            if (!mkdir("../products")) {
                throw new Exception(" * failed to create folder ../product");
            }
        }

        if (!is_dir("../products/$this->product_name")) {
            if (!mkdir("../products/$this->product_name")) {
                throw new Exception(" * failed to create folder ../products/$this->product_name");
            }
        }

        $result = @move_uploaded_file($soruce_path, $str_path);

        if (!$result) {
            throw new Exception(" * Faield to uplaod file");
        }
    }
      private function set_product_discount($product_discount) {

        if (!is_numeric($product_discount) || $product_discount <= 0) {
            throw new Exception(" * Invalid/Missing product discount");
        }
        $this->product_discount = $product_discount;
    }

    private function get_product_discount() {
        return $this->product_discount;
    }
    public function add_product(){
        $obj_db=  $this->obj_db();

       
        $query="INSERT INTO `products` (`productID`,`product_name`,`product_image`,"
                . "`description`,`proto_description`,`quantity`,`product_discount`,"
                . "`product_price`,`brandID`,`categoryID`)"
                . "VALUES"
                . "('$this->productID','$this->product_name','$this->product_image',"
                . "'$this->description','$this->proto_description',"
                . "'$this->quantity','$this->product_discount','$this->product_price','$this->brand','$this->category')";
    
        $result=$obj_db->query($query);
        if($obj_db->errno)
        {
            throw new Exception("Product Insert Error $obj_db->error>> $obj_db->errno");
        }
        $this->productID=$obj_db->insert_id;
    }
     public function get_products($categoryID=1,$start=-1,$count=0) {

        $obj_db = self::obj_db();
//        $query = "select * "
//                . "from products_full "
//                . "where productID = '$this->productID'";

        $query = "
SELECT products.`productID`,products.product_name, products.`product_image`,products.`product_price`, products.`product_discount`, products.`proto_description`,products.`description`,  products.`quantity`, products.brandID, products.`view_count`, products.categoryID FROM products LEFT JOIN category ON category.categoryID = products.categoryID WHERE category.`categoryID`=$categoryID";
        //***************************************************************


        if ($start > -1 && $count > 0) {
            $query .= " limit $start, $count";
        }

        $result = $obj_db->query($query);
//echo"<pre>";
//print_r($query);
//echo "</pre>";
        if ($obj_db->errno) {
            throw new Exception(" * Product selection Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception(" * Product not found");
        }
$products=array();
        while ($data = $result->fetch_object())
        {
//if($view_update)
//{
// $data->view_count++;
//$query_update="UPDATE products SET view_count = view_count+1 where productID = '$this->productID'";
//   
//}
//  $result2 = $obj_db->query($query_update);
//            if ($obj_db->affected_rows == 0) {
//                throw new Exception("Nothing view cout updated");
//            }
            $temp=new Product();
        $temp->productID = $data->productID;
        $temp->product_name = $data->product_name;
        $temp->description = $data->description;
        $temp->proto_description=$data->proto_description;
        $temp->product_image = $data->product_image;
        $temp->product_price = $data->product_price;
        $temp->quantity = $data->quantity;
        $temp->view_count = $data->view_count;
       
        $temp->product_discount = $data->product_discount;
        
        $products[] = $temp;
        }      
     return $products;
//        echo "<pre>";
//        print_r($products);
//        echo "</pre>";
       
}
 public function get_products_for_admin($start=-1,$count=0) {

        $obj_db = self::obj_db();
//        $query = "select * "
//                . "from products_full "
//                . "where productID = '$this->productID'";

        $query = "
SELECT * from products";
        //***************************************************************


        if ($start > -1 && $count > 0) {
            $query .= " limit $start, $count";
        }

        $result = $obj_db->query($query);
//echo"<pre>";
//print_r($query);
//echo "</pre>";
        if ($obj_db->errno) {
            throw new Exception(" * Product selection Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception(" * Product not found");
        }
$products=array();
        while ($data = $result->fetch_object())
        {
//if($view_update)
//{
// $data->view_count++;
//$query_update="UPDATE products SET view_count = view_count+1 where productID = '$this->productID'";
//   
//}
//  $result2 = $obj_db->query($query_update);
//            if ($obj_db->affected_rows == 0) {
//                throw new Exception("Nothing view cout updated");
//            }
            $temp=new Product();
        $temp->productID = $data->productID;
        $temp->product_name = $data->product_name;
        $temp->description = $data->description;
        $temp->proto_description=$data->proto_description;
        $temp->product_image = $data->product_image;
        $temp->product_price = $data->product_price;
        $temp->quantity = $data->quantity;
        $temp->view_count = $data->view_count;
       
        $temp->product_discount = $data->product_discount;
        
        $products[] = $temp;
        }      
     return $products;
//        echo "<pre>";
//        print_r($products);
//        echo "</pre>";
       
}
//public function get_search_product($productID ){
//    $obj_db=  self::obj_db();
//    $query="SELECT * FROM products WHERE productID=$productID";
//    $result=$obj_db->query($query);
//    if($obj_db->errno){
//        throw new Exception("Product selection error $obj_db->errno>>>>>>>>>.$obj_db->error");
//    }
//    if($result->num_rows==0){
//        throw new Exception("Product is Not Found $this->product_name");
//    }
//      $product=array();
//  $data=$result->fetch_object();
//  if($view_count) {
//      $data->view_count++;
//      $query_update="UPDATE products SET view_count=view_count+1 WHERE productID=$productID";
//     $res=$obj_db->query($query_update);
//     
//     if($obj_db->affected_rows==0)
//     {
//         throw new Exception("No change");
//     }
//  }
//       $temp=new Product();
//       $temp->productID=$data->productID;
//       $temp->product_name=$data->product_name;
//       $temp->product_image=$data->product_image;
//       $temp->description=$data->description;
//       $temp->product_price=$data->product_price;
//       $temp->product_discount=$data->product_discount;
//       $temp->quantity=$data->quantity;
//       $temp->proto_description=$data->proto_description;
//       
//   
//   $product[]=$temp;
//  
//   return $product;
//   
//}
public function get_single_product($productID ,$view_count=FALSE){
    $obj_db=  self::obj_db();
    $query="SELECT * FROM products WHERE productID=$productID";
    $result=$obj_db->query($query);
    if($obj_db->errno){
        throw new Exception("Product selection error $obj_db->errno>>>>>>>>>.$obj_db->error");
    }
    if($result->num_rows==0){
        throw new Exception("Product is Not Found $this->product_name");
    }
    
   $product=array();
  $data=$result->fetch_object();
  if($view_count) {
      $data->view_count++;
      $query_update="UPDATE products SET view_count=view_count+1 WHERE productID=$productID";
     $res=$obj_db->query($query_update);
     
     if($obj_db->affected_rows==0)
     {
         throw new Exception("No change");
     }
  }
       $temp=new Product();
       $temp->productID=$data->productID;
       $temp->product_name=$data->product_name;
       $temp->product_image=$data->product_image;
       $temp->description=$data->description;
       $temp->product_price=$data->product_price;
       $temp->product_discount=$data->product_discount;
       $temp->quantity=$data->quantity;
       $temp->proto_description=$data->proto_description;
       
   
   $product[]=$temp;
  
   return $product;
   
}
 public static function pagination($item_per_page = 6,$categoryID=11) {
        $obj_db = self::obj_db();

        $query = "Select count(*) 'count' from `products` where categoryID='$categoryID'";
        $result = $obj_db->query($query);


        if ($result->num_rows == 0) {
            throw new Exception(" * Product(s) not found");
        }

        $data = $result->fetch_object();
        $total_items = $data->count;

        $page_count = ceil($total_items / $item_per_page);

        $page_nums = array();

        for ($i = 1, $j = 0; $i <= $page_count; $i++, $j+=$item_per_page) {
            $page_nums[$i] = $j;
        }
return$page_nums;
//        echo "<pre>";
//        print_r($page_nums);
//        echo "</pre>";
    }
    public static function pagination_for_admin($item_per_page = 6) {
        $obj_db = self::obj_db();

        $query = "Select count(*) 'count' from `products`";
        $result = $obj_db->query($query);


        if ($result->num_rows == 0) {
            throw new Exception(" * Product(s) not found");
        }

        $data = $result->fetch_object();
        $total_items = $data->count;

        $page_count = ceil($total_items / $item_per_page);

        $page_nums = array();

        for ($i = 1, $j = 0; $i <= $page_count; $i++, $j+=$item_per_page) {
            $page_nums[$i] = $j;
        }
return$page_nums;
//        echo "<pre>";
//        print_r($page_nums);
//        echo "</pre>";
    }
    public static function get_index_product($action)
    {
        $obj_db= self::obj_db();
        $query="SELECT * FROM products";
       
        switch ($action) {
            case "new_arrival":
$query.=" ORDER BY productID DESC LIMIT 3";

                break;
                case "special":
                    $query.=" ORDER BY product_discount DESC LIMIT 3";
                   
                    break;
                    case "fashion":
                        $query.=" WHERE categoryID=10 OR categoryID=13 ORDER BY productID DESC LIMIT 3";
                        break;
                        case "electronic":
                            $query.=" WHERE categoryID=1 OR categoryID=3 ORDER BY productID DESC LIMIT 3 ";
                            break;
            case "clearance_sale":
                $query.=" ORDER BY product_discount desc limit 3";
                            default:
                                $query="  ORDER BY productID ASC LIMIT 3";
                break;
        }
     $result=$obj_db->query($query);
       
        if($obj_db->errno){
            throw new Exception("Product not selected $obj_db->error >> $obj_db->errno");
        }
       $products=array();
        while ($data = $result->fetch_object())
        {

            $temp=new Product();
        $temp->productID = $data->productID;
        $temp->product_name = $data->product_name;
        $temp->description = $data->description;
        $temp->proto_description=$data->proto_description;
        $temp->product_image = $data->product_image;
        $temp->product_price = $data->product_price;
        $temp->quantity = $data->quantity;
        $temp->view_count = $data->view_count;
         $temp->category=$data->categoryID;
        $temp->product_discount = $data->product_discount;
        
        $products[] = $temp;
        }      
     return $products;
//        echo "<pre>";
//        print_r($products);
//        echo "</pre>";
       
    }
        public function update_product($keypro) {
//        $str_path = "../products/$this->product_name/$this->product_image";
//        
//        $result = @move_uploaded_file($soruce_path, $str_path);
//
//        if (!$result) {
//            throw new Exception(" * Faield to uplaod file");
//        }
        
        $obj_db = $this->obj_db();
        
        $query_update = "update `products` set "
                . "product_name='".$_POST['product_name']."',"
                
                . "product_discount ='" . $_POST['product_discount'] . "',"
                . "product_price ='" . $_POST['product_price'] . "',"
                . "quantity ='" . $_POST['quantity'] . "',"
                . "proto_description ='" . $_POST['proto_description'] . "'"
                . " where `productID` = $keypro";

//        echo $query_update;
//        die;
        $r4 = $obj_db->query($query_update);

        if ($obj_db->affected_rows == 0) {
            throw new Exception(" * Update Product information error <br> Nothing Update");
        }
    }
    public function delete_product($key){
        $obj_db=  self::obj_db();
        $query="delete from products where productID=$key";
        $result=$obj_db->query($query);
        if($obj_db->affected_rows==0){
            throw new Exception("Nothing Delete");
        }
        
        
    }
}

