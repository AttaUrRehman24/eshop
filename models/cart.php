<?php
require_once "item.php";
require_once "user.php";
require_once "DB_Connection.php";

class Cart extends DB_Connection{
    
    private $items;
    private $user;


    public function __construct() {
        $this->items = array();
        $this->user = new User();
        
    }
    
    public function __set($name, $value) {
        $method_name = "set_$name";

        if (!method_exists($this, $method_name)) {
            throw new Exception("SET Property $name does not exist");
        }

        $this->$method_name($value);
    }

    public function __get($name) {
        $method_name = "get_$name";

        if (!method_exists($this, $method_name)) {
            throw new Exception("GET Property $name does not exist");
        }

        return $this->$method_name();
    }
    private function get_items(){
        
        if(!$this->count){
            
            return FALSE;
        }
        return $this->items;
    }
    
        private function get_count(){
            $total = 0;
            foreach ($this->items as $item){
                $total+=$item->quantity;
            }
            return $total;
        }
        private function get_total_price(){
            $total=0;
            foreach ($this->items as $item){
                $total +=$item->total_price;
            }
            return $total;
        }
         public function add_to_cart($item){
        if(!$item instanceof Item){
            throw new Exception("Item must be of type ITEM");
        }
        
        if(array_key_exists($item->itemID, $this->items)){
            $this->items[$item->itemID]->quantity += $item->quantity;
        }
        else{
            $this->items[$item->itemID] = $item;
        }
        
    }
    
    public function remove_item($item){
        if(!$item instanceof Item){
            throw new Exception("Item must be of type ITEM");
        }
        
        if(array_key_exists($item->itemID, $this->items)){
            unset($this->items[$item->itemID]);
        }
        
    }
    
    public function update_cart($qtys){
      
        echo("<pre>");
        print_r($this->items);
        echo("</pre>");
        echo("2<pre>");
        print_r($qtys);
        echo("</pre>");
        $i = 0;
        foreach($this->items as $item){
            
            $item->quantity = $qtys[$i];
            $i++;
        }
        
        echo("<pre>");
        print_r($this->items);
        echo("</pre>");
       
 
        foreach($this->items as $item){
            
            if(is_numeric($qtys[$item->itemID])){
                if($qtys[$item->itemID] > 0){
                    $item->quantity = $qtys[$item->itemID];
                }
                elseif($qtys[$item->itemID] == 0){
                    $this->remove_item($item);
                }
            }
        }
    }
    
    public function empty_cart(){
        $this->items = array();
    }
    
    
    public function add_cart() {
        $obj_db = $this->obj_db();
//        
//        
//        
//        $product = array();
//        array_push($product,$qtys[$item->itemID]);
//        serialize($product);
//        $thisuser = $this->user->userID;
//        echo $thisuser;
//       
//        
//        $query = "INSERT INTO `cart`"
//                . "(`UserId`, `ProductId`, `Quantity`) "
//                . "VALUES "
//                . "('$this->user', '$product', '$this->quantity');";
//
//        $result = $obj_db->query($query);
//
//        if ($obj_db->errno) {
//            throw new Exception(" * New Cart Insert Error - $obj_db->error -$obj_db->errno");
//        }
//
//        $this->cartID = $obj_db->insert_id;
//    }   
//    

}
}