<?php

require_once "DB_Connection.php";
class Brand extends DB_Connection{
    
    private $brandID;
    private $brand_name;
    private $brand_image;
    private $is_active;
     public function __construct() {
    
    }

    public function __set($name, $value) {
        $method_name = "set_$name";

        if (!method_exists($this, $method_name)) {
            throw new Exception(" * SET Property $name does not exist");
        }

        $this->$method_name($value);
    }

    public function __get($name) {
        $method_name = "get_$name";

        if (!method_exists($this, $method_name)) {
            throw new Exception(" * GET Property $name does not exist");
        }

        return $this->$method_name();
    }

    private function set_brandID($brandID) {

        if (!is_numeric($brandID) || $brandID <= 0) {
            
            throw new Exception(" * Invalid/Missing brandID");
        }
        $this->brandID = $brandID;
    }

    private function get_brandID() {
   
        return $this->brandID;
    }

    
    private function set_brand_name($brand_name) {
        $reg = "/^[a-z]+$/i";
        if (!preg_match($reg, $brand_name)) {
            throw new Exception(" * Invalid/Missing Brand name");
        }
        $this->brand_name = strtolower($brand_name);
    }

    private function get_brand_name() {
        return $this->brand_name;
    }

    private function set_brand_image($brand_image) {

        if ($brand_image['error'] == 4) {
            throw new Exception(" * File Missing");
        }

        $image = getimagesize($brand_image['tmp_name']);

        if (!$image) {
            throw new Exception(" * not a valid image");
        }

        if ($brand_image['size'] > 10000000000) {
            throw new Exception(" * max file size allowed is 1000 K");
        }

     

        if ($brand_image['type'] != $image['mime']) {
            throw new Exception(" * Corrupt image");
        }

        if (is_null($this->brand_name)) {
            throw new Exception(" * Failed to generate file name");
        }

        $path_info = pathinfo($brand_image['name']);
        extract($path_info);

        $this->brand_image = $this->brand_name . "." . $extension;
    }

    


    public function upload_brand_image($soruce_path) {
        $str_path = "../brands/$this->brand_name/$this->brand_image";
        if (!is_dir("../brands")) {
            if (!mkdir("../brands")) {
                throw new Exception(" * failed to create folder ../brands");
            }
        }

        if (!is_dir("../brands/$this->brand_name")) {
            if (!mkdir("../brands/$this->brand_name")) {
                throw new Exception(" * failed to create folder ../brands/$this->brand_name");
            }
        }

        $result = @move_uploaded_file($soruce_path, $str_path);

        if (!$result) {
            throw new Exception(" * Faield to uplaod file");
        }
    }
    
    public function add_brand($act_code){
        $obj_db=  $this->obj_db();
         $query = "INSERT INTO `brands` "
                . "(`brandID`, `brand_name`, `brand_image`, "
                . "`reset_code`) "
                . "VALUES "
                . "('$this->brandID', '$this->brand_name', '$this->brand_image', "
                . "'$act_code');";
        $result= $obj_db->query($query);
        
        if ($obj_db->errno) {
            throw new Exception(" * New Brand Insert Error - $obj_db->error -$obj_db->errno");
        }

        $this->brandID = $obj_db->insert_id;
    }
     public function activate_brand($act_code) {

        $obj_db = $this->obj_db();

        $query = "select register_time, is_active  "
                . "from `brands` "
                . "where brandID = '$this->brandID' "
                . "and reset_code = '$act_code' "
                . "and delete_state = 0";

        $result = $obj_db->query($query);

        if ($obj_db->errno) {
            throw new Exception(" * Select Activate Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception(" * Activation Data Not Found");
        }
   
        $query_update = "UPDATE brands SET is_active=1,reset_code=0 WHERE brandID=$this->brandID ;";
        $reult = $obj_db->query($query_update);

        if ($obj_db->affected_rows == 0) {
            throw new Exception(" * Nothing Update");
        }
     
}
    public static function get_brand($keybrand){
        $obj_db = self::obj_db();
        $query = "select * from brands"
                . " where brandID = '$keybrand'";
        
//        echo $query;
//        die;
        
        $result = $obj_db->query($query);
        if ($obj_db->errno) {
            throw new Exception(" * Brand selection Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception(" * Brand (s) not found");
        }
        $brand = array();
        while($data = $result->fetch_object()){
            $temp = new self();
            $temp->brandID = $data->brandID;
            $temp->brand_name = $data->brand_name;
            $temp->brand_image = $data->brand_image;
            $temp->is_active = $data->is_active;
            $brand[] = $temp;
        }
        return $brand;
    }
    
    public static function get_brands(){
        $obj_db = self::obj_db();
        $query = "select * from brands";
        
        $result = $obj_db->query($query);
        
        $brands = array();
        while($data = $result->fetch_object()){
            $temp = new self();
            $temp->brandID = $data->brandID;
            $temp->brand_name = $data->brand_name;
            $temp->brand_image = $data->brand_image;
            $temp->is_active = $data->is_active;
            $brands[] = $temp;
        }
        return $brands;
    }
    
    public static function get_Index_brands(){
        $obj_db = self::obj_db();
        $query = "select * from brands LIMIT 5";
        
        $result = $obj_db->query($query);
        
        $brands = array();
        while($data = $result->fetch_object()){
            $temp = new self();
            $temp->brandID = $data->brandID;
            $temp->brand_name = $data->brand_name;
            $temp->brand_image = $data->brand_image;
            $brands[] = $temp;
        }
        return $brands;
    }

    public static function get_low_Index_brands(){
        $obj_db = self::obj_db();
        $query = "select * from brands LIMIT 4";
        
        $result = $obj_db->query($query);
        
        $brands = array();
        while($data = $result->fetch_object()){
            $temp = new self();
            $temp->brandID = $data->brandID;
            $temp->brand_name = $data->brand_name;
            $temp->brand_image = $data->brand_image;
            $brands[] = $temp;
        }
        return $brands;
    }
    
    public function delete_brand($key) {

        $obj_db = $this->obj_db();

        $query = "delete from `brands` "
                . "where brandID = '$key' ";


        $result = $obj_db->query($query);

        if ($obj_db->affected_rows == 0) {
            throw new Exception(" * Nothing Update");
        }
    }
    
    public function update_brand($key) {
        $obj_db = $this->obj_db();

        
        $query_update = "UPDATE brands SET "
                . " brand_name='".$_POST['brand_name']."',"
                . " where `brandID` = $key";

//        echo $query_update;
//        die;
        $r4 = $obj_db->query($query_update);

        if ($obj_db->affected_rows == 0) {
            throw new Exception(" * Update brand information error <br> Nothing Update");
        }
    }

        }