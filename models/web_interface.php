<?php
abstract class Web_Interface{
    public static function load_genders($gender=Null){
        $genders=array("Male","Female","Other");
        $output="";
        foreach ($genders as $i){
        
                
       $output.= "<input type='radio'  name='gender' id='$i' value='$i' class'validate' ";
       if($i== $gender){
                    $output .="checked";
                }
              $output .= "> ";
        $output.="<label for='$i'>$i</label>";
      
        }
        echo "$output";
    }
    public static function load_interests($interest){
        $interests=array("Football","Hockey" ,"Cricket");
        $output="";
        foreach ($interests as $i)
        {
            $output.="<input type='checkbox' id='$i' name='interest[]' value='$i'";
                  if ($i == $interest) {
                $output .= "checked";
            }
                  $output.= "> ";
               $output .="<label for='$i'>$i</label>&nbsp;&nbsp; &nbsp;  ";
        }
        echo "$output";
    }
//    public static function load_countries($country){
//        $output="";
//        $output.="<select class='form-control' id='country' name='country' >";
//        $output.="<option value='Countries'>Countries</option>";
//        $countries=array("Afghanistan",
//	"Albania",
//	"Algeria",
//	"American Samoa",
//	"Andorra",
//	"Angola",
//	"Anguilla",
//	"Antarctica",
//	"Antigua and Barbuda",
//	"Argentina",
//	"Armenia",
//	"Aruba",
//	"Australia",
//	"Austria",
//	"Azerbaijan",
//	"Bahamas",
//	"Bahrain",
//	"Bangladesh",
//	"Barbados",
//	"Belarus",
//	"Belgium",
//	"Belize",
//	"Benin",
//	"Bermuda",
//	"Bhutan",
//	"Bolivia",
//	"Bosnia and Herzegovina",
//	"Botswana",
//	"Bouvet Island",
//	"Brazil",
//	"British Indian Ocean Territory",
//	"Brunei Darussalam",
//	"Bulgaria",
//	"Burkina Faso",
//	"Burundi",
//	"Cambodia",
//	"Cameroon",
//	"Canada",
//	"Cape Verde",
//	"Cayman Islands",
//	"Central African Republic",
//	"Chad",
//	"Chile",
//	"China",
//	"Christmas Island",
//	"Cocos (Keeling) Islands",
//	"Colombia",
//	"Comoros",
//	"Congo",
//	"Congo, the Democratic Republic of the",
//	"Cook Islands",
//	"Costa Rica",
//	"Cote D'Ivoire",
//	"Croatia",
//	"Cuba",
//	"Cyprus",
//	"Czech Republic",
//	"Denmark",
//	"Djibouti",
//	"Dominica",
//	"Dominican Republic",
//	"Ecuador",
//	"Egypt",
//	"El Salvador",
//	"Equatorial Guinea",
//	"Eritrea",
//	"Estonia",
//	"Ethiopia",
//	"Falkland Islands (Malvinas)",
//	"Faroe Islands",
//	"Fiji",
//	"Finland",
//	"France",
//	"French Guiana",
//	"French Polynesia",
//	"French Southern Territories",
//	"Gabon",
//	"Gambia",
//	"Georgia",
//	"Germany",
//	"Ghana",
//	"Gibraltar",
//	"Greece",
//	"Greenland",
//	"Grenada",
//	"Guadeloupe",
//	"Guam",
//	"Guatemala",
//	"Guinea",
//	"Guinea-Bissau",
//	"Guyana",
//	"Haiti",
//	"Heard Island and Mcdonald Islands",
//	"Holy See (Vatican City State)",
//	"Honduras",
//	"Hong Kong",
//	"Hungary",
//	"Iceland",
//	"India",
//	"Indonesia",
//	"Iran, Islamic Republic of",
//	"Iraq",
//	"Ireland",
//	"Israel",
//	"Italy",
//	"Jamaica",
//	"Japan",
//	"Jordan",
//	"Kazakhstan",
//	"Kenya",
//	"Kiribati",
//	"Korea, Democratic People's Republic of",
//	"Korea, Republic of",
//	"Kuwait",
//	"Kyrgyzstan",
//	"Lao People's Democratic Republic",
//	"Latvia",
//	"Lebanon",
//	"Lesotho",
//	"Liberia",
//	"Libyan Arab Jamahiriya",
//	"Liechtenstein",
//	"Lithuania",
//	"Luxembourg",
//	"Macao",
//	"Macedonia, the Former Yugoslav Republic of",
//	"Madagascar",
//	"Malawi",
//	"Malaysia",
//	"Maldives",
//	"Mali",
//	"Malta",
//	"Marshall Islands",
//	"Martinique",
//	"Mauritania",
//	"Mauritius",
//	"Mayotte",
//	"Mexico",
//	"Micronesia, Federated States of",
//	"Moldova, Republic of",
//	"Monaco",
//	"Mongolia",
//	"Montserrat",
//	"Morocco",
//	"Mozambique",
//	"Myanmar",
//	"Namibia",
//	"Nauru",
//	"Nepal",
//	"Netherlands",
//	"Netherlands Antilles",
//	"New Caledonia",
//	"New Zealand",
//	"Nicaragua",
//	"Niger",
//	"Nigeria",
//	"Niue",
//	"Norfolk Island",
//	"Northern Mariana Islands",
//	"Norway",
//	"Oman",
//	"Pakistan",
//	"Palau",
//	"Palestinian Territory, Occupied",
//	"Panama",
//	"Papua New Guinea",
//	"Paraguay",
//	"Peru",
//	"Philippines",
//	"Pitcairn",
//	"Poland",
//	"Portugal",
//	"Puerto Rico",
//	"Qatar",
//	"Reunion",
//	"Romania",
//	"Russian Federation",
//	"Rwanda",
//	"Saint Helena",
//	"Saint Kitts and Nevis",
//	"Saint Lucia",
//	"Saint Pierre and Miquelon",
//	"Saint Vincent and the Grenadines",
//	"Samoa",
//	"San Marino",
//	"Sao Tome and Principe",
//	"Saudi Arabia",
//	"Senegal",
//	"Serbia and Montenegro",
//	"Seychelles",
//	"Sierra Leone",
//	"Singapore",
//	"Slovakia",
//	"Slovenia",
//	"Solomon Islands",
//	"Somalia",
//	"South Africa",
//	"South Georgia and the South Sandwich Islands",
//	"Spain",
//	"Sri Lanka",
//	"Sudan",
//	"Suriname",
//	"Svalbard and Jan Mayen",
//	"Swaziland",
//	"Sweden",
//	"Switzerland",
//	"Syrian Arab Republic",
//	"Taiwan, Province of China",
//	"Tajikistan",
//	"Tanzania, United Republic of",
//	"Thailand",
//	"Timor-Leste",
//	"Togo",
//	"Tokelau",
//	"Tonga",
//	"Trinidad and Tobago",
//	"Tunisia",
//	"Turkey",
//	"Turkmenistan",
//	"Turks and Caicos Islands",
//	"Tuvalu",
//	"Uganda",
//	"Ukraine",
//	"United Arab Emirates",
//	"United Kingdom",
//	"United States",
//	"United States Minor Outlying Islands",
//	"Uruguay",
//	"Uzbekistan",
//	"Vanuatu",
//	"Venezuela",
//	"Viet Nam",
//	"Virgin Islands, British",
//	"Virgin Islands, US",
//	"Wallis and Futuna",
//	"Western Sahara",
//	"Yemen",
//	"Zambia",
//	"Zimbabwe");
//    foreach ($countries as $i){
//        $output.="<option value='$i'";
//        if($i==$country){
//                $output.= "selected";
//        }
//                $output.= ">$i</option>";
//        
//    }
//    $output.="</select>";
//    echo "$output";
//    }
//    public static function load_payment_menthod($payment_menthod) {
//
//        $payment_menthods = array("PayPal", "Master card", "Vise card");
//        $output = "";
//
//        foreach ($payment_menthods as $i) {
//        
//                
//       $output.= "<input type='radio'  name='payment_menthod' id='$i' value='$i' class'validate' ";
//       if($i== $payment_menthod){
//                    $output .="checked";
//                }
//              $output .= "> ";
//        $output.="<label for='$i'>$i</label>";
//      
//        }
//     
//        echo($output);
//    }
    
     public static function load_product_features($product_features) {

        $iOptions = array("Android","IOS","Windows","6th Generation","5th Generation","15.9 inches");
        $output = "";

        foreach ($iOptions as $i) {
            $output .= "$i <input type='checkbox' name='product_features[]' id='$i' value='$i'";

            if (in_array($i, $product_features)) {
                $output .= " checked";
            }
            $output .= "> - ";
        }
        echo($output);
    }
        public static function load_payment_menthod($payment_menthod) {

        $payment_menthods = array("PayPal", "master card", "vise card");
        $output = "";

        foreach ($payment_menthods as $i) {
            $output .= "$i <input type='radio' name='payment_menthod' id='$i' value='$i'";
            if ($i == $payment_menthod) {
                $output .= " checked";
            }
            $output .= "> - ";
        }
        echo($output);
    }
    public function load_country(){
        $host = "localhost";
        $user = "root";
        $password = "";
        $database = "eshoop";
        
        
        $obj_db = new mysqli();
        
        $obj_db->connect($host, $user, $password);
        
        if($obj_db->connect_errno){
            throw new Exception(" * DB Conenct Error - $obj_db->connect_error -$obj_db->connect_errno");
        }

        $obj_db->select_db($database);
        
        if($obj_db->errno){
            throw new Exception(" * DB Select Error - $obj_db->error -$obj_db->errno");
        }
    
    $query="select * from countries ";
    

    $result=$obj_db->query($query);
           
   echo"<select id='country' name='country'>";
        while ($res=$result->fetch_object()){
            echo"<option value=".$res->country_id."> ".$res->country_name." </option>";
        }
        echo "</select>";
    
    }
    
}