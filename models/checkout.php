<?php

require_once 'DB_Connection.php';
class Checkout extends DB_Connection {

    private $payment_menthod;
    private $zipcode;
    private $customer_note;
    private $contact_number;

    public function __construct() {
        
    }

    public function __set($name, $value) {
        $method_name = "set_$name";

        if (!method_exists($this, $method_name)) {
            throw new Exception(" * SET Property $name does not exist");
        }

        $this->$method_name($value);
    }

    public function __get($name) {
        $method_name = "get_$name";

        if (!method_exists($this, $method_name)) {
            throw new Exception(" * GET Property $name does not exist");
        }

        return $this->$method_name();
    }

    private function set_payment_menthod($payment_menthod) {

        $payment_menthods = array("PayPal", "Master card", "Vise card");
        if (!in_array($payment_menthod, $payment_menthods)) {
            throw new Exception(" * Missing payment Menthod");
        }
        $this->payment_menthod = $payment_menthod;
    }

    private function get_payment_menthod() {
        return $this->payment_menthod;
    }

    private function set_contact_number($contact_number) {

        //$reg = "/^\d{1,4}\-\d{3}\-\d{7}$/";
        ///starting from 4 digits then - then 7 digits
        $reg = "/^\d{4}?-?\d{7}/";

        if (!preg_match($reg, $contact_number)) {
            throw new Exception(" * Invalid/Missing Contact Number");
        }

        $this->contact_number = $contact_number;
    }

    private function get_contact_number() {
        return $this->contact_number;
    }

    private function set_zipcode($zipcode) {

        $reg = "/^\d{5}/";

        if (!preg_match($reg, $zipcode)) {
            throw new Exception(" * Invalid/Missing Zip Code");
        }

        $this->zipcode = $zipcode;
    }

    private function get_zipcode() {
        return $this->zipcode;
    }
    
    private function set_customer_note($customer_note) {

        $customer_note = trim($customer_note);
        if (empty($customer_note) || strlen($customer_note) < 10) {
            throw new Exception(" * Missing/Too Short Customer Suggestion ");
        }
        $this->customer_note = $customer_note;
    }

    private function get_customer_note() {
        return $this->customer_note;
    }
    
    
    
}
