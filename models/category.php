<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'DB_Connection.php';
class Category extends DB_Connection{
    private $categoryID;
    private $category_name;
    
    public function __construct() {
        
    }
    public function __set($name, $value) {
        $method_name="set_$name";
        if (!method_exists($this, $method_name)){
            throw new Exception("The Set Property of $name is not exsit");
        }
        $this->$method_name($value);
    }
    public function __get($name) {
        $method_name="get_$name";
        if(!method_exists($this, $method_name))
        {
            throw new Exception("The Get Propery of $name is not exsit");
        }
        return$this->$method_name();
    }
    private function set_categoryID($categoryID){
        if(is_numeric($categoryID)|| $categoryID<=0){
            throw new Exception("The Category ID is Invalid or Empty");
        }
        $this->categoryID=$categoryID;
    }
    private function get_categoryID(){
        return $this->categoryID;
    }
    public function set_category_name($category_name)
    {
        $reg="/^[a-z\s]+$/i";
        if(!preg_match($reg, $category_name))
        {
            throw new Exception("Invalid Or Empty Category Name");
        }
        $this->category_name=$category_name;
    }
    public function get_category_name(){
        return$this->category_name;
    }

    public function add_category(){
        $obj_db=  $this->obj_db();
        $query="INSERT INTO `category` (`categoryID`,`category_name`) VALUES ('$this->categoryID','$this->category_name')";
    $result= $obj_db->query($query);
    if($obj_db->errno)
    {
        throw new Exception("New Category error $obj_db->errno >>>$obj_db->error...");
    }
    $this->categoryID=$obj_db->insert_id;
    }
    public static function get_category(){
        $obj_db=  self::obj_db();
        $query="SELECT * FROM category";
        $result=$obj_db->query($query);
        if($obj_db->errno)
        {
            throw new Exception("Category select error $obj_db->errno ..$obj_db->errno ....");
        }
        $categorys=array();
        while ($data=$result->fetch_object()){
            $temp=new self();
            $temp->categoryID=$data->categoryID;
            $temp->category_name=$data->category_name;
            $categorys[]=$temp;
        }
        return $categorys;
    }
       public static function get_index_categories(){
        $obj_db = self::obj_db();
        $query = "select * from category limit 5";
        
        $result = $obj_db->query($query);
        
        if ($obj_db->errno) {
            throw new Exception(" * Category selection Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception(" * Category(s) not found");
        }
        
        $categorys = array();
        while($data = $result->fetch_object()){
            $temp = new self();
            $temp->categoryID = $data->categoryID;
            $temp->category_name = $data->category_name;
            $categorys[] = $temp;
        }
        return $categorys;
    }
    
    public function delete_category($key){
        $obj_db=  $this->obj_db();
        $query="DELETE FROM `category` WHERE categoryID='$key'";
           $result = $obj_db->query($query);

        if ($obj_db->affected_rows == 0) {
            throw new Exception(" * Nothing Update");
        }
    }
}
