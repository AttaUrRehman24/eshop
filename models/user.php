<?php
require_once 'DB_Connection.php';

class User extends DB_Connection
{
    private $first_name;
    private $last_name;
    private $email;
    private $user_name;
    private $password;
    private $confirm_password;
    private $contact;
    private $gender;
    private $interest;
    private $date;
    private $country;
    private $city;
    private $state;
    private $address;
    private $profile_image;
    private $userID;
     private $login_status;
    private $is_active;
    private $signup_date;

    public function __construct() {
      $this->interest = array();
    }
    public function __set($name, $value) {
        $method_name="set_$name";
        if(!method_exists( $this , $method_name))
        {
            throw new Exception("The set Property of $name is not found");
        }
        $this->$method_name($value);
    }
    public function __get($name) {
        $method_name="get_$name";
        if(!method_exists($this, $method_name)){
            throw new Exception("The get Property of $name is not found");
        }
         return$this->$method_name();
    }
    private function set_first_name($first_name){
       
        $reg="/^[a-z\s]+$/i";
        if(!preg_match($reg, $first_name))
        {
            throw new Exception("First Name Invalid");
        }
        
        $this->first_name=$first_name;
    }
    
    private function get_first_name(){
        
       
        return $this->first_name;
    }
    private function set_last_name($last_name){
        $reg="/^[a-z\s]+$/i";
        if(!preg_match($reg, $last_name))
        {
            throw new Exception("Last Name Invalid");
        }
        $this->last_name=$last_name;
    }
    private function get_last_name(){
        return$this->last_name;
    }
    private function set_email($email)
            {
        $reg="/^[a-z]+(-|\.)?[0-9|a-z_]*@[a-z]+\.(com|pk|org)*$/i";
        if(!preg_match($reg, $email))
                {
            throw new Exception("Invalid Email");
                }
                $this->email=$email;
            }
            private function get_email(){
                return$this->email;
            }
            private function set_user_name($user_name)
            {
                $reg="/^[a-z\s]+(-|\.)?[0-9|a-z]+?$/i";
                if(!preg_match($reg,$user_name)){
                    throw new Exception("Invalid User Name");
                }
                $this->user_name=$user_name;
            }
            private function get_user_name(){
                return$this->user_name;
            }
            private function set_password($password){
                $reg="/^[a-z|0-9]+(-|\.)?$/i";
                if(!preg_match($reg, $password)){
                throw new Exception("Invalid Password");
                }
                $this->password=$password;
}
private function get_password(){
    return$this->password;
}
private function set_confirm_password($confirm_password){
     if(empty($confirm_password)|| $this->password !=$confirm_password)
         {
        throw new Exception("Password is not Matched Or empty");
    }
    
    $this->confirm_password=$confirm_password;
}
private function get_confirm_password(){
    return$this->confirm_password;
}


private function set_contact($contact){
    $reg="/^[0-9]+$/i";
    if(!preg_match($reg, $contact)){
        throw new Exception("Invalid Number");
    }
    $this->contact=$contact;
}private function get_contact(){
    return$this->contact;
}
private function set_gender($gender){
    $genders=  array("Male","Female","Other");
    if (!in_array($gender, $genders)){
        throw new Exception("Gender Missing");
        
    }
    $this->gender=$gender;
}
private function get_gender(){
     
    return$this->gender;
}
    private function set_interest($interest) {
        if(!is_array($interest)){
            throw new Exception("Missing Interset");
        }
 $iOptions = array("Football","Hockey" ,"Cricket");

        foreach ($interest as $i) {
            if (!in_array($i, $iOptions)) {
                throw new Exception(" * Invalid Interests Option(s)");
            }
        }
                $this->interest=$interest;
            }
private function get_interest(){
    return$this->interest;
}

private function set_date($date){
    $reg= "/^\d{2}\/\d{2}\/\d{4}$/";
    if(!preg_match($reg, $date))
    {
        throw new Exception("Invalid Format");
    }
   $parts=  explode("/", $date);
   list($month,$day,$year)=$parts;
  if(!isset($day) || !isset($month) || !isset($year)){
            throw new Exception("Invalid Date of Birth");
        }


        if(!checkdate($month, $day, $year))
        {
            throw new Exception("Invalid Pattern");
        }
        $this->date = mktime(0,0,0,$month,$day,$year);
}
private function get_date() {
   if(is_null($this->date))
    {
       return 0;
    }
    $date=date("d-m-y", $this->date);
    return$date;
}
private function set_state($state){
    if(empty($state))
    {
        throw new Exception("Missing State");
    }
    $this->state=$state;
}
private function get_state() {
        return $this->state;
    }
    private function set_city($city){
    if(empty($city))
    {
        throw new Exception("Missing City");
    }
    $this->city=$city;
}
private function get_city() {
        return $this->city;
    }
    private function set_address($address){
        if(empty($address))
        {
            throw new Exception("Missing Street Address");
        }
        if(!strlen($address<20))
        {
            throw new Exception("Too Short Address");
        }
        $this->address=$address;
    }
    private function get_address(){
        return$this->address;
    }
    private function set_country($country) {
        $c_list=array("Afghanistan",
	"Albania",
	"Algeria",
	"American Samoa",
	"Andorra",
	"Angola",
	"Anguilla",
	"Antarctica",
	"Antigua and Barbuda",
	"Argentina",
	"Armenia",
	"Aruba",
	"Australia",
	"Austria",
	"Azerbaijan",
	"Bahamas",
	"Bahrain",
	"Bangladesh",
	"Barbados",
	"Belarus",
	"Belgium",
	"Belize",
	"Benin",
	"Bermuda",
	"Bhutan",
	"Bolivia",
	"Bosnia and Herzegovina",
	"Botswana",
	"Bouvet Island",
	"Brazil",
	"British Indian Ocean Territory",
	"Brunei Darussalam",
	"Bulgaria",
	"Burkina Faso",
	"Burundi",
	"Cambodia",
	"Cameroon",
	"Canada",
	"Cape Verde",
	"Cayman Islands",
	"Central African Republic",
	"Chad",
	"Chile",
	"China",
	"Christmas Island",
	"Cocos (Keeling) Islands",
	"Colombia",
	"Comoros",
	"Congo",
	"Congo, the Democratic Republic of the",
	"Cook Islands",
	"Costa Rica",
	"Cote D'Ivoire",
	"Croatia",
	"Cuba",
	"Cyprus",
	"Czech Republic",
	"Denmark",
	"Djibouti",
	"Dominica",
	"Dominican Republic",
	"Ecuador",
	"Egypt",
	"El Salvador",
	"Equatorial Guinea",
	"Eritrea",
	"Estonia",
	"Ethiopia",
	"Falkland Islands (Malvinas)",
	"Faroe Islands",
	"Fiji",
	"Finland",
	"France",
	"French Guiana",
	"French Polynesia",
	"French Southern Territories",
	"Gabon",
	"Gambia",
	"Georgia",
	"Germany",
	"Ghana",
	"Gibraltar",
	"Greece",
	"Greenland",
	"Grenada",
	"Guadeloupe",
	"Guam",
	"Guatemala",
	"Guinea",
	"Guinea-Bissau",
	"Guyana",
	"Haiti",
	"Heard Island and Mcdonald Islands",
	"Holy See (Vatican City State)",
	"Honduras",
	"Hong Kong",
	"Hungary",
	"Iceland",
	"India",
	"Indonesia",
	"Iran, Islamic Republic of",
	"Iraq",
	"Ireland",
	"Israel",
	"Italy",
	"Jamaica",
	"Japan",
	"Jordan",
	"Kazakhstan",
	"Kenya",
	"Kiribati",
	"Korea, Democratic People's Republic of",
	"Korea, Republic of",
	"Kuwait",
	"Kyrgyzstan",
	"Lao People's Democratic Republic",
	"Latvia",
	"Lebanon",
	"Lesotho",
	"Liberia",
	"Libyan Arab Jamahiriya",
	"Liechtenstein",
	"Lithuania",
	"Luxembourg",
	"Macao",
	"Macedonia, the Former Yugoslav Republic of",
	"Madagascar",
	"Malawi",
	"Malaysia",
	"Maldives",
	"Mali",
	"Malta",
	"Marshall Islands",
	"Martinique",
	"Mauritania",
	"Mauritius",
	"Mayotte",
	"Mexico",
	"Micronesia, Federated States of",
	"Moldova, Republic of",
	"Monaco",
	"Mongolia",
	"Montserrat",
	"Morocco",
	"Mozambique",
	"Myanmar",
	"Namibia",
	"Nauru",
	"Nepal",
	"Netherlands",
	"Netherlands Antilles",
	"New Caledonia",
	"New Zealand",
	"Nicaragua",
	"Niger",
	"Nigeria",
	"Niue",
	"Norfolk Island",
	"Northern Mariana Islands",
	"Norway",
	"Oman",
	"Pakistan",
	"Palau",
	"Palestinian Territory, Occupied",
	"Panama",
	"Papua New Guinea",
	"Paraguay",
	"Peru",
	"Philippines",
	"Pitcairn",
	"Poland",
	"Portugal",
	"Puerto Rico",
	"Qatar",
	"Reunion",
	"Romania",
	"Russian Federation",
	"Rwanda",
	"Saint Helena",
	"Saint Kitts and Nevis",
	"Saint Lucia",
	"Saint Pierre and Miquelon",
	"Saint Vincent and the Grenadines",
	"Samoa",
	"San Marino",
	"Sao Tome and Principe",
	"Saudi Arabia",
	"Senegal",
	"Serbia and Montenegro",
	"Seychelles",
	"Sierra Leone",
	"Singapore",
	"Slovakia",
	"Slovenia",
	"Solomon Islands",
	"Somalia",
	"South Africa",
	"South Georgia and the South Sandwich Islands",
	"Spain",
	"Sri Lanka",
	"Sudan",
	"Suriname",
	"Svalbard and Jan Mayen",
	"Swaziland",
	"Sweden",
	"Switzerland",
	"Syrian Arab Republic",
	"Taiwan, Province of China",
	"Tajikistan",
	"Tanzania, United Republic of",
	"Thailand",
	"Timor-Leste",
	"Togo",
	"Tokelau",
	"Tonga",
	"Trinidad and Tobago",
	"Tunisia",
	"Turkey",
	"Turkmenistan",
	"Turks and Caicos Islands",
	"Tuvalu",
	"Uganda",
	"Ukraine",
	"United Arab Emirates",
	"United Kingdom",
	"United States",
	"United States Minor Outlying Islands",
	"Uruguay",
	"Uzbekistan",
	"Vanuatu",
	"Venezuela",
	"Viet Nam",
	"Virgin Islands, British",
	"Virgin Islands, US",
	"Wallis and Futuna",
	"Western Sahara",
	"Yemen",
	"Zambia",
	"Zimbabwe");
        if(!in_array($country, $c_list)){
            throw new Exception("Invalid Choice");
        }
        $this->country=$country;
    }

    private function get_country() {
        return $this->country;
        
    }
  private function set_profile_image($profile_image) {

        if ($profile_image['error'] == 4) {
            throw new Exception(" * File Missing");
        }

        $image = getimagesize($profile_image['tmp_name']);

        if (!$image) {
            throw new Exception(" * not a valid image");
        }

        if ($profile_image['size'] > 10000000000) {
            throw new Exception(" * max file size allowed is 1000 K");
        }

         if ($profile_image['type'] != "image/jpeg") {
            throw new Exception(" * Only jpeg allowed");
        }

        if ($profile_image['type'] != $image['mime']) {
            throw new Exception(" * Corrupt image");
        }

        if (is_null($this->user_name)) {
            throw new Exception(" * Failed to generate file name");
        }

        $path_info = pathinfo($profile_image['name']);
        extract($path_info);

        $this->profile_image = $this->user_name . "." . $extension;
    }

    private function get_profile_image() {
       
        return $this->profile_image;
    }
     public function upload_profile_image($source_path){
              
        $str_path = "../users/$this->user_name/$this->profile_image";
       
        
        if(!is_dir("../users")){
            if(!mkdir ("../users")){
                throw new Exception("Failed to Create Dir");
            }
        }
        
        if(!is_dir("../users/$this->user_name")){
            if(!mkdir("../users/$this->user_name")){
                throw new Exception("Failed to Create User Dir");
            }
        }
        //die($str_path);
        $result = @move_uploaded_file($source_path, $str_path);

        if(!$result){
            throw new Exception("Failed to upload file");
        }
    }
    private function set_userID($userID){
        if(!is_numeric($userID))
        {
            throw new Exception("user id error");
        }
        $this->userID=$userID;
    }
    private function get_userID(){
    return$this->userID;
    }

       public function add_user($act_code) {
  $obj_db = $this->obj_db();
       $query = "INSERT INTO `users` "
                . "(`userID`, `first_name`, `last_name`, "
                . "`email`, `user_name`, `password`, `gender`, `interest`, "
                . "`contact`, `dob`, `address`, "
                . "`city`, `state`, `country`, `profile_image`, "
                . "`reset_code`) "
                . "VALUES "
                . "('$this->userID', '$this->first_name', "
                . " '$this->last_name', "
                . "'$this->email', '$this->user_name', '$this->password', "
                . "'$this->gender', '" . serialize($this->interest) . "', "
                . "'$this->contact', '$this->date', "
                . "'$this->address', '$this->city', '$this->state', "
                . "'$this->country', '$this->profile_image', "
                . "'$act_code');";

        $result = $obj_db->query($query);
        
        if ($obj_db->errno) {
            throw new Exception(" * New User Insert Error - $obj_db->error -$obj_db->errno");
        }
        if ($obj_db->errno == 1062) {
            throw new Exception("user alredy exist");
}

        $this->userID = $obj_db->insert_id;
    }

    public function activate_user($act_code) {

        $obj_db = $this->obj_db();
$query_select = "SELECT userID, is_active, reset_code,register_time,delete_state=0"
                . " FROM"
                . " users"
                . " WHERE"
                . " userID= '$this->userID'"
                . " AND"
                . " reset_code= '$act_code'";
    
            //die($query_select);
        $result= $obj_db->query($query_select);
        
        if($obj_db->errno){
            throw new Exception("USER Select Error ==>".$obj_db->errno);
            
        }
        
        if($result->num_rows==0){
            throw new Exception("Invalid Activation Code");
        }
        $data = $result->fetch_object();
       
        if($data->is_active){
            throw new Exception("You are already activated");
        }
        $signup_date= strtotime($data->register_time);
        $expiry_time= $signup_date+(60*60*24);
        $now= time();
        
        if($now > $expiry_time){
            $query_delete = "DELETE FROM"
                    . " users"
                    . " WHERE"
                    . " userID= $this->userID";
            $result = $obj_db->query($query_delete);
            if($obj_db->errno){
                throw new Exception("USER Delete Error ==>".$obj_db->errno);

            } 
            if($obj_db->affected_rows==0){
                throw new Exception("Nothing Deleted");
            }
            throw new Exception("Your time has Expire sign in again");
        }
        $query_update="UPDATE users"
                . " SET"
                . " is_active= 1,"
                . " reset_code= NULL"
                . " WHERE"
                . " userID= $this->userID";
        //die($query_update);
        $result = $obj_db->query($query_update);
        if($obj_db->errno){
            throw new Exception("USER Update Error ==>".$obj_db->errno);

        } 
        if($obj_db->affected_rows==0){
            throw new Exception("Nothing Updated");
        }
        
        
        
    }
       public function login($remember = FALSE) {

        $obj_db = $this->obj_db();

        $query = "select userID, user_name, email, first_name, "
                . "last_name, gender ,contact  "
                . "from `users` "
                . "where user_name = '$this->user_name' "
                . "and password = '$this->password' "
                . "and delete_state = 0";

        $result = $obj_db->query($query);

        if ($obj_db->errno) {
            throw new Exception(" * Select Login Error - $obj_db->error -$obj_db->errno");
        }

        if ($result->num_rows == 0) {
            throw new Exception(" * Access denied !! "
             
            . "or email us "
            . "iamatta2416@gmail.com");
        }

        $data = $result->fetch_object();

        $this->userID = $data->userID;
        $this->email = $data->email;
        $this->contact = $data->contact;
        $this->gender = $data->gender;
        $this->user_name = $data->user_name;
        $this->address = $data->address;
        $this->state = $data->state;
        $this->city = $data->city;
        $this->first_name = $data->first_name;
        $this->last_name = $data->last_name;
        $this->login_status = TRUE;
      

        $str_user = serialize($this);
     

        $_SESSION['obj_user'] = $str_user;
        
        if ($remember) {
            $expire = time() + (60 * 60 * 24 * 15);
            setcookie("obj_user", $str_user, $expire, "/");
        }
    }
       public function logout() {
        if (isset($_SESSION['obj_user'])) {
            unset($_SESSION['obj_user']);
        }
       
    }
  
    private function get_is_active() {
        return $this->is_active;
    }

    private function get_signup_date() {
        return $this->signup_date;
    }

    private function get_login() {
        return $this->login_status;
    }
    
    
    
    public static function get_users(){
        $obj_db=  self::obj_db();
        $query="SELECT * FROM `users`";
        $result = $obj_db->query($query);
        
        if($obj_db->errno)
        {
            throw new Exception("Database selection error $obj_db->errno>>>>>>$obj_db->error");
        }
    
        $users=array();
        while ($data=$result->fetch_object()){
            $temp=new User();
            $temp->userID=$data->userID;
            $temp->first_name=$data->first_name;
        $temp->last_name=$data->last_name;
        $temp->user_name=$data->user_name;
        $temp->email=$data->email;
        $temp->gender=$data->gender;
        $temp->interest=$data->interest;
        $temp->date=$data->dob;
        $temp->city=$data->city;
        $temp->country=$data->country;
        $temp->is_active=$data->is_active;
        $temp->profile_image=$data->profile_image;
        $temp->state=$data->state;
        $temp->address=$data->address;
        $temp->contact=$data->contact;
           $users[]=$temp; 
        }
        
        return $users;
    }

    public static function get_UserInfo($key){
        $obj_db= self::obj_db();
        $query="select * from `users`"
                . "where userID =$key";
        $result=$obj_db->query($query);
        if($obj_db->errno){
            throw new Exception("User selection Error $obj_db->errno >>> $obj_db->error");
        }
        if ($result->num_rows == 0) {
            throw new Exception(" * user (s) not found");
        }
        $users=array();
        while($data=$result->fetch_object()){
            $temp=new User();
            $temp->userID=$data->userID;
            $temp->first_name=$data->first_name;
        $temp->last_name=$data->last_name;
        $temp->user_name=$data->user_name;
        $temp->contact=$data->contact;
        $temp->email=$data->email;
        $temp->gender=$data->gender;
        $temp->date=$data->dob;
        $temp->city=$data->city;
        $temp->country=$data->country;
        $temp->is_active=$data->is_active;
        $temp->profile_image=$data->profile_image;
        $temp->state=$data->state;
        $temp->address=$data->address;
        
           $users[]=$temp; 
        }
        return $users;
    }
    public function delete_User($key){
        $obj_db= $this->obj_db();
        $query="DELETE FROM `users`"
                . "where userID ='$key'";
        $result=$obj_db->query($query);
        if($obj_db->errno)
        {
            throw new Exception("ERRor in delete $obj_db->errno>>>>>>>$obj_db->error");
        }
        if($obj_db->affected_rows==0)
        {
            throw new Exception("User not delete");
        }
        
    }
    public function update_user($key){
        $obj_db=  $this->obj_db();
        $query_update = "UPDATE users SET first_name = '".$_POST["first_name"]."' , last_name = '".$_POST["last_name"]."' , contact = '".$_POST["contact"]."' , email = '".$_POST["email"]."' , gender = '".$_POST["gender"]."' ,user_name = '".$_POST["user_name"]."' WHERE userID = $key";
        echo $query_update;
        //die;
     // echo "<pre>";       
      //print_r($_POST);
      //echo "</pre>";   
      //die;

              $result=$obj_db->query($query_update);

        if($obj_db->errno){
            throw new Exception("Update Connection Error$obj_db->errno>>> $obj_db->error");
        }
        if($obj_db->affected_rows==0)
        {
            throw new Exception("No User Deleted");
        }
        
                
    }
    public function update_profile_image($soruce_path) {
        
        $str_path = "../users/$this->user_name/$this->profile_image";
             
        if(!is_dir("../users")){
            if(!mkdir ("../users")){
                throw new Exception("Failed to Create Dir");
            }
        }
        
        if(is_dir("../users/$this->user_name")){
            if(rmdir("../users/$this->user_name")){
                throw new Exception("Failed to Create User Dir");
            }
        }
        $result = @move_uploaded_file($soruce_path, $str_path);

        if (!$result) {
            throw new Exception(" * Faield to uplaod file");
        }
        
    
}
    public function update_user_pass($key) {
        $obj_db = $this->obj_db();

        $query_update = "update `users` set "
                . "password ='".$_POST['password']."'"
                . "where `userID` = $key";

        echo $query_update;
        die;

        $r4 = $obj_db->query($query_update);

        if ($obj_db->affected_rows == 0) {
            throw new Exception(" * Update user information error <br> Nothing Update");
        }
    }
  
   
}