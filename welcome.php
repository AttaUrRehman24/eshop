<?php 
 require_once ("models/user.php");
 require_once 'models/category.php';
 require_once 'models/brand.php';
 require_once 'models/product.php';
 require_once 'models/checkout.php';
 require_once 'models/cart.php';
 require_once 'models/web_interface.php';
 require_once ("views/top.php");
?>
<title>Home | Welcome</title>
    </head>
    <body>
        <?php
       
       require_once ("views/header_top.php");
       require_once ("views/middle_header.php");
       require_once ("views/bootom_header.php");
       ?>
        <div class="page_body">
            <h2 class=" h2">Welcome</h2>
                <?php 
          if(isset($_SESSION['msg'])){
                            $msg = $_SESSION['msg'];
                            unset($_SESSION['msg']);
                            echo($msg);
                if (isset($_SESSION['msg_email'])) {
                echo($_SESSION['msg_email'] . "<hr>");
                unset($_SESSION['msg_email']);
            }

            if (isset($_SESSION['msg_err'])) {
                echo($_SESSION['msg_err'] . "<hr>");
                unset($_SESSION['msg_err']);
            }
    }
        ?>
        </div>
        <?php
       require_once ("views/footer.php");
        ?>
        </div>
        <!--FOOTER ENDS -->
  <?php     require_once("./translate/js.php")      ?>
<script src="js/bootstrap.min.js" type="text/javascript"></script>
 <script src="js/materialize.min.js" type="text/javascript"></script>
<script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script>
$(window).scroll(function(){
console.log($(window).scrollTop());
if($(window).scrollTop()>=200)
                {
                    if ($(window).width() >768){
		$(".collapse").css({"position":"fixed","margin-top":"-206px","z-index":"2","border-bottom":"4px solid #03a9f4","padding-top":"30px","background-color":"white","width":"100%"});
		
                
                }
            
                else
                {
                    $(".navbar-collapse").css({"position":"fixed","margin-top":"-250px","z-index":"2","background-color":"white","width":"100%"});
                $(".navbar").css({"position":"fixed","margin-top":"-300px","z-index":"2","background-color":"white","width":"100%"});
            }
            }
            else{
                $(".collapse").css({"position":"relative","margin-top":"-50px","border-bottom":"none","padding-bottom":"20px","padding-top":"45px","z-index":"1","background-color":"transparent"});
            $(".navbar").css({"position":"relative","margin-top":"auto","z-index":"1","background-color":"transparent"});
        }   
        });
        </script>
               
    </body>
</html>
